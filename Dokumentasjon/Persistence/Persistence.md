# PERSISTENCE
## Files
The program requires 3 files for storage:
- [PLAYERDATA.DTA](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Persistence/playerdata.md)
-    [TEAMDATA.DTA](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Persistence/teamdata.md)
-    [TOURNAMENTDATA.DTA](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Persistence/tournamentdata.md)

The files need to be located within the same folder as the TournamentPro executable.
TournamentPro can go into an infinite loop if the datafiles are formatted incorrectly.
All files use ‘;’ to separate data.
