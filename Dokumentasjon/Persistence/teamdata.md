### TEAMDATA.DTA
This file includes all registered teams information, including member players gamertag
As the amount of members a team can have is unlimited, the string “EndOfTeamMembers” ends reading of players.
Up to 4 registered players are written per line (including EndOfTeamMembership).

```
<number of teams>
<nr. of players on team>;<team name>;<team nationality>;
<tournaments played>;<tournaments won>;<matches played>;<matches won>;
<registered player 1>;<registered player 2>;<registered player 3>;<registered player 4>;
<registered player 5>;<EndOfTeamMembers>;
<nr. of players on team>;<team name>;<team nationality>;
<tournaments played>;<tournaments won>;<matches played>;<matches won>;
<registered player 1>;<registered player 2>;<registered player 3>;<registered player 4>;
<registered player 5>;<registered player 6>;<registered player 7>;<registered player 8>;
<EndOfTeamMembers>;

and so on...
```

### TEAMDATA.DTA Example

```
16
5;Hellbears;Norway;
3;2;7;6;
AndersAnd;BjBear;Cornelio;DidgeriDoodle;
Eriandel;EndOfTeamMembers;
0;Centaurs;USA;
4;0;4;0;
EndOfTeamMembers;
2;Tomatoes;Norway;
4;0;5;1;
Greed;Heart;EndOfTeamMembers;
5;Newerth Heroes;Newerth;
0;0;0;0;
Link;PrincessPeach;Voldemort;FantasticMoose;
MarioSupreme;EndOfTeamMembers;
5;Battered Bulls;Spain;
0;0;0;0;
BjBear;Krill;J-Son;Heart;
Greed;EndOfTeamMembers;
5;FreedomFighters;Ukraine;
0;0;0;0;
Achoom;Eriandel;Greed;AndersAnd;
Cornelio;EndOfTeamMembers;
6;Sisters and Brothers;Fantasyland;
0;0;0;0;
DidgeriDoodle;FantasticMoose;Achoom;MissAndromeda;
Link;Dumbledore;EndOfTeamMembers;
4;Amazons;Brazil;
0;0;0;0;
MissAndromeda;PrincessPeach;Greed;J-Son;
EndOfTeamMembers;
3;Ducktown Dodgers;Disneyland;
0;0;0;0;
Link;PrincessPeach;Heart;EndOfTeamMembers;
3;Flyaways;airland;
0;0;0;0;
Cornelio;Eriandel;IceIce;EndOfTeamMembers;
3;RepulsiveRadgers;unknown;
0;0;0;0;
Doomguy;Achoom;IceIce;EndOfTeamMembers;
5;MirrorVerse;mirrorverse;
0;0;0;0;
LuigiIsTheBetterBrother;MarioSupreme;Link;J-Son;
AndersAnd;EndOfTeamMembers;
3;BokerOgBorst;Norway;
0;0;0;0;
Dumbledore;Voldemort;Krill;EndOfTeamMembers;
4;Hepburns;Canada;
0;0;0;0;
FantasticMoose;FooFighter;BjBear;AndersAnd;
EndOfTeamMembers;
5;HooRahs;USA;
0;0;0;0;
IceIce;Cornelio;Achoom;MarioSupreme;
BjBear;EndOfTeamMembers;
5;Hullabaloobahs;Imaginaryland;
0;0;0;0;
Eriandel;FooFighter;Achoom;Krill;
FantasticMoose;EndOfTeamMembers;

```
