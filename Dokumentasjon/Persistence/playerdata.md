### PLAYERDATA.DTA
This file includes all registered players information
As the amount of teams a player can be registered to is unlimited, the string “EndOfTeamMembership” ends reading of teams.
Up to 4 teams are written per line (including EndOfTeamMembership).

```
<number of players>
<player1 name >;<player1 gamertag>;<player1 nationality>;
<tournaments played>;<tournaments won>;<matches played>;<matches won>;
<team membership 1>;<team membership 2>;<team membership 3>;<team membership4>;
<team membership 5>;<EndOfTeamMembership>;
<player2 name >;<player2 gamertag>;<player2 nationality>;
<tournaments played>;<tournaments won>;<matches played>;<matches won>;
<team membership 1>;<team membership 2>;<team membership 3>;<team membership4>;
<team membership 5>;<team membership 6>;<team membership 7>;<team membership 8>; <EndOfTeamMembership>;

And so on...
```

 
### PLAYERDATA.DTA EXAMPLE

```
23
Anders Andersen;AndersAnd;Norway;
9;2;18;11;
Hellbears;FreedomFighters;MirrorVerse;Hepburns;
EndOfTeamMembership;
Bjarte Bjorn;BjBear;Denmark;
7;2;11;6;
Hellbears;Battered Bulls;Hepburns;HooRahs;
EndOfTeamMembership;
Chris Cornell;Cornelio;Hungary;
7;2;12;7;
Hellbears;FreedomFighters;Flyaways;HooRahs;
EndOfTeamMembership;
Didrik Dork;DidgeriDoodle;Norway;
3;2;7;6;
Hellbears;Sisters and Brothers;EndOfTeamMembership;
Eirin Ellingsen;Eriandel;Norway;
3;2;7;6;
Hellbears;FreedomFighters;Flyaways;Hullabaloobahs;
EndOfTeamMembership;
Fredrik;FooFighter;Norway;
0;0;0;0;
Hepburns;Hullabaloobahs;EndOfTeamMembership;
Geir;Greed;Norway;
4;1;8;5;
Tomatoes;Battered Bulls;FreedomFighters;Amazons;
EndOfTeamMembership;
Hallgeir;Heart;Norway;
0;0;0;0;
Tomatoes;Battered Bulls;Ducktown Dodgers;EndOfTeamMembership;
el;IceIce;Norway;
0;0;0;0;
Flyaways;RepulsiveRadgers;HooRahs;EndOfTeamMembership;
go Kurosaki;J-Son;Norway;
0;0;0;0;
Battered Bulls;Amazons;MirrorVerse;EndOfTeamMembership;
san;Krill;Japan;
0;0;0;0;
Battered Bulls;BokerOgBorst;Hullabaloobahs;EndOfTeamMembership;
Elgesen;FantasticMoose;Norway;
0;0;0;0;
Newerth Heroes;Sisters and Brothers;Hepburns;Hullabaloobahs;
EndOfTeamMembership;
Uluk Yusuf;Achoom;Uruguay;
0;0;0;0;
FreedomFighters;Sisters and Brothers;RepulsiveRadgers;HooRahs;
Hullabaloobahs;EndOfTeamMembership;
Audrey Hepburn;MissAndromeda;England;
0;0;0;0;
Sisters and Brothers;Amazons;EndOfTeamMembership;
Luigi;LuigiIsTheBetterBrother;Italy;
0;0;0;0;
MirrorVerse;EndOfTeamMembership;
Mario;MarioSupreme;Italy;
0;0;0;0;
Newerth Heroes;MirrorVerse;HooRahs;EndOfTeamMembership;
h;PrincessPeach;Bowsers castle;
0;0;0;0;
Newerth Heroes;Amazons;Ducktown Dodgers;EndOfTeamMembership;
;Link;Hyrule;
0;0;0;0;
Newerth Heroes;Sisters and Brothers;Ducktown Dodgers;MirrorVerse;
EndOfTeamMembership;
Albus Dumbledore;Dumbledore;Hogwarts;
0;0;0;0;
Sisters and Brothers;BokerOgBorst;EndOfTeamMembership;
Tom Marvolo Riddle;Voldemort;Great Britain;
0;0;0;0;
Newerth Heroes;BokerOgBorst;EndOfTeamMembership;
Doomguy;Doomguy;Unknown;
0;0;0;0;
RepulsiveRadgers;EndOfTeamMembership;
Repulsive Repulsor;DrRepulsor;Newerth;
0;0;0;0;
EndOfTeamMembership;
Minny Mouse;Magical;Disneyland;
0;0;0;0;
EndOfTeamMembership;

```
