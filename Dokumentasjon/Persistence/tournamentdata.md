### TOURNAMENTDATA.DTA
This file includes all registered tournament information, including either the name of a participating team, or the gamertag of a participating player.
All tournamentbrackets are stored whether they have a participant or not, if no player or team is registered the value is stored as "IkkeDefinert".
the bool variable <match played> signifies if a match has been played or not.

Tournaments of sizes 8 and smaller will disregard the first 16 matches.
Tournaments of size 4 will also disregard the following 8 matches.

```
<number of tournaments>
<tournamentName>;<teambased (bool)>;<tournament size>;
<roundmatches played>;<quarterfinal matches played>;<semifinal matches played>;<grandfinal matches played>;
<Roundmatch 1.1>;<Roundmatch 1.2>;<match played (bool)>;<Roundmatch 2.1>;<Roundmatch 2.2>;<match played (bool)>;
<Roundmatch 3.1>;<Roundmatch 3.2>;<match played (bool)>;<Roundmatch 4.1>;<Roundmatch 4.2>;<match played (bool)>;
<Roundmatch 5.1>;<Roundmatch 5.2>;<match played (bool)>;<Roundmatch 6.1>;<Roundmatch 6.2>;<match played (bool)>;
<Roundmatch 7.1>;<Roundmatch 7.2>;<match played (bool)>;<Roundmatch 8.1>;<Roundmatch 8.1>;<match played (bool)>;
<Quarterfinal 1.1>;<Quarterfinal 1.2><match played (bool)>;;<Quarterfinal 2.1>;<Quarterfinal 2.2>;<match played (bool)>;
<Quarterfinal 3.1>;<Quarterfinal 3.2><match played (bool)>;;<Quarterfinal 4.1>;<Quarterfinal 4.2>;<match played (bool)>;
<Semifinal 1.1>;<Semifinal 1.2>;<match played (bool)>;<Semifinal 2.1>;<Semifinal 2.2>;<match played (bool)>;
<Grandfinal 1.1>;<Grandfinal 1.2>;<match played (bool)>;
<tournamentName>;<teambased (bool)>;<tournament size>;
<roundmatches played>;<quarterfinal matches played>;<semifinal matches played>;<grandfinal matches played>;
<Roundmatch 1.1>;<Roundmatch 1.2>;<match played (bool)>;<Roundmatch 2.1>;<Roundmatch 2.2>;<match played (bool)>;
and so on...

```

### TOURNAMENTDATA.DTA Example

```
2
PlayerCup16;0;16;
8;4;0;0;
Achoom;MissAndromeda;1;LuigiIsTheBetterBrother;MarioSupreme;1;
Link;Voldemort;1;DrRepulsor;Magical;1;
Greed;AndersAnd;1;BjBear;Cornelio;1;
DidgeriDoodle;Eriandel;1;FantasticMoose;Krill;1;
Achoom;MarioSupreme;1;Link;Magical;1;
Greed;Cornelio;1;DidgeriDoodle;FantasticMoose;1;
MarioSupreme;Magical;0;Greed;DidgeriDoodle;0;
IkkeDefinert;IkkeDefinert;0;
PlayerCup4;0;4;
8;4;2;1;
IkkeDefinert;IkkeDefinert;0;IkkeDefinert;IkkeDefinert;0;
IkkeDefinert;IkkeDefinert;0;IkkeDefinert;IkkeDefinert;0;
IkkeDefinert;IkkeDefinert;0;IkkeDefinert;IkkeDefinert;0;
IkkeDefinert;IkkeDefinert;0;IkkeDefinert;IkkeDefinert;0;
IkkeDefinert;IkkeDefinert;0;IkkeDefinert;IkkeDefinert;0;
IkkeDefinert;IkkeDefinert;0;IkkeDefinert;IkkeDefinert;0;
Dumbledore;Voldemort;1;Doomguy;DrRepulsor;1;
Dumbledore;DrRepulsor;1;

```

