- [Product Manual](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/productManual.md)
   - [Welcome screen](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/welcomeScreen.md)
     - [Spill turnering](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/spillTurnering.md)
       - [**Spill Neste match**](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/spillTurneringNeste.md)
       - [Liste opp gjenværende matcher for runde](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/spillTurneringListeOpp.md)
       - [se turneringsbracket](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/spillTurneringSeBracket.md)


### Bilde:
![spillTurneringSpillNeste.png](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/Images/spillTurneringSpillNeste.png)

### Bruk:
After choosing which tournament will play, the user can now set up matches. In the picture the user has started a quarterfinale. The quarterfinale consists of two round matches and one finale. The first round match is started and user can choose player 1 and player 2. After that user will type in which player won the match. User will be asked to either type inn an arbitrary characther to continue to the next tournament round, or Q to quit. User types inn "e" to continue. Round match 2 begins, and user can again types inn exactly the same as inn round match 1. User is asked again asked to type inn an arbitrary characther to continue to the next tournament round, or Q to quit. User types inn "g" and continues to the finale. Same as previous user chooses players and who won the match. After the winner is selected it will print out congratulations and the name of the winner, and that the tournament is over. The user will get a meny with four new options, S, L, T and Q. S is play next match, L is list of remaining matches for round, T is see tournamentbracket and Q is return.

### Andre ting som skjer?
For every match played: 
The winning participant has their "MatchesPlayed" and "MatchesWon" stat increased by 1.
The losing participant has thei "MatchesPlayed" stat increased by 1.
    
The winning participant is moved into the next tier bracket, waiting for that match to be played.

If the participant is a team, both the team and teammembers "MatchesPlayed" and "MatchesWon" stats are affected. 
If the match being played is the grand final, the "TournamentsPlayed" and "TournamentsWon" will also be increased respectively.
