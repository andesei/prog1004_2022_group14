 - [Product Manual](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/productManual.md)
    - [Administrasjon av turneringer og deltakere](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/administrasjon.md)
       - [Turnering](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admTurnering.md)
          - [Ny turnering](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admTurneringNy.md)
          - [Vis alle turneringer](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admTurneringVis.md)
          - [**Endre turnering**](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admTurneringEndre.md)
             - [Legge til deltaker](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admTurneringEndreLeggTil.md)
             - [Slette deltaker](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admTurneringEndreSletteDeltaker.md)
             - [Vis alle deltakere](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admTurneringEndreVisDeltakere.md)
             - [Vis turneringsformat](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admTurneringEndreVisBracket.md)
             - [Endre turneringens navn](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admTurneringEndreNavn.md)
          - [Slette turnering](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admTurneringSlett.md)

### Bilde: 

![Endre Turnering.png](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/Images/admTurneringEndre.png)

### Bruk: 
Alle registrerte turneringer skrives ut som en liste. Bruker bes så skrive inn navn på turneringen det ønskes å modifisere. Etterpå skrives submeny:

 - L: Legge til deltaker
 - S: Slette deltaker
 - D: Vis alle deltakere
 - V: Vis turneringsformat
 - E: Endre turneringens navn
 - Q: Avslutt

### Andre ting som skjer?
Ingenting.
