- [Product Manual](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/productManual.md)
   - [Welcome screen](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/welcomeScreen.md)
     - [**Spill turnering**](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/spillTurnering.md)
       - [Spill Neste match](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/spillTurneringNeste.md)
       - [Liste opp gjenværende matcher for runde](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/spillTurneringListeOpp.md)
       - [se turneringsbracket](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/spillTurneringSeBracket.md)


### Bilde:
![spillTurnering.png](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/Images/spillTurnering.png)

### Bruk:
Here the user choose option S where you can choose which tournament is going to play. It prints out all torunaments that have been added with details. As you can see in the picture it prints out all tournemants, if it is a singel-player or multi-player tournemant and how many players it is on the team. User is asked what tournament is going to play and types in the name of the team that will play. 

- S: Spill neste match
- L: Liste opp gjenværende matcher for runde
- T: Se turneringsbracket
- Q: Returner

### Andre ting som skjer?
Ingenting.
