 - [Product Manual](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/productManual.md)
    - [Administrasjon av turneringer og deltakere](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/administrasjon.md)
       - [Turnering](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admTurnering.md)
          - [**Ny turnering**](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admTurneringNy.md)
          - [Vis alle turneringer](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admTurneringVis.md)
          - [Endre turnering](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admTurneringEndre.md)
             - [Legge til deltaker](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admTurneringEndreLeggTil.md)
             - [Slette deltaker](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admTurneringEndreSletteDeltaker.md)
             - [Vis alle deltakere](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admTurneringEndreVisDeltakere.md)
             - [Vis turneringsformat](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admTurneringEndreVisBracket.md)
             - [Endre turneringens navn](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admTurneringEndreNavn.md)
          - [Slette turnering](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admTurneringSlett.md)

### Bilde: 

 ![spillTurnering.png](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/Images/admTurneringNy.png)

### Bruk: 
Bruker bes skrive inn navn til turneringen, etterfulgt av å definere turneringen for enkeltspillere eller lag.
Videre må bruker skrive inn hvor mange deltakere turneringen skal ha, dvs størrelsen på turneringen.

Videre modifikasjon av turneringen må gjøres via 'Endre turnering', hvor bl.a deltakere legges inn/fjernes osv.
Spilling av turneringen skjer via 'Spill Turnering' i hovedmenyen.

### Andre ting som skjer?
turneringsobjekt dannes og datamedlemmer fylles av constructor.
