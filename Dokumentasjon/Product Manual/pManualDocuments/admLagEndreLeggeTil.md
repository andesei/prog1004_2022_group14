- [Product Manual](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/productManual.md)
  - [Administrasjon av turneringer og deltakere](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/administrasjon.md)
       - [Lag](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admLag.md)
          - [Nytt lag](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admLagNy.md)
          - [Vis alle lag](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admLagVis.md)
          - [Endre lag](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admLagEndre.md)
             - [Vis lagets spillere](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admLagEndreVisSpillere.md)
             - [Endre lagnavn](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admLagEndreNavn.md)
             - [Endre nasjonalitet](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admLagEndreNasjonalitet.md)
             - [**Legge til spiller i lag**](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admLagEndreLeggeTil.md)
             - [Slette spiller fra lag](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admLagEndreSlett.md)
          - [Slette lag](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admLagSlett.md)

### Bilde:
Ta nytt screenshot, User input ikke kommet med på screenshot.
![Legge til spiller.png](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/Images/admLagEndreLeggTil.png)

### Bruk:
Alle programmets registrerte spiller blir listet opp. Bruker blir så bedt om å skrive inn GamerTagen til spilleren han vil legge til i laget. 
Bruker fortsetter å legge inn spillere helt til det skrives inn "Q".

### Andre ting som skjer?
lagobjektet oppdateres med spillere som blir lagt til, og spillerens eget objekt bli oppdatert med lagmedlemskapet.
