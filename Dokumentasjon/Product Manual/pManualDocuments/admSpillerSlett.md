- [Product Manual](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/productManual.md)
  - [Administrasjon av turneringer og deltakere](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/administrasjon.md)
    - [Spiller](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admSpiller.md)
      - [Ny spiller](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admSpillerNy.md)
      - [Vis alle spillere](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admSpillerVis.md)
      - [Vis alle spillere med Detaljer](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admSpillerVisDetaljert.md)
      - [Endre spillerdetaljer](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admSpillerEndre.md)
         - [Endre spillers fødenavn](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admSpillerEndreNavn.md)
         - [Endre spillers nasjonalitet](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admSpillerEndreNasjonalitet.md)
         - [Endre GamerTag](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admSpillerEndreGamerTag.md)
      - [**Slette spiller**](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admSpillerSlett.md)

### Bilde: 
![spillTurnering.png](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/Images/admSpillerSlett.png)

### Bruk:
Etter at delmenyen Slett spiller er valgt listes alle registrerte spillere opp.
Bruker må så skrive inn en listet GamerTag for å slette gitt spiller fra programmet. 

Brukeren vil bli bedt om en ekstra bekreftelse på om spilleren ønskes slettet.

### Andre ting som skjer?
En spiller som slettes vil automatisk også bli fjernet fra alle lag og turneringer han/hun er registrert i.

