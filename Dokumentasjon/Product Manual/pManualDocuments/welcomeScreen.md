- [Product Manual](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/productManual.md)
   - [**Welcome screen**](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/welcomeScreen.md)
     - [Spill turnering](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/spillTurnering.md)
     - [Vis Turneringer](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/visTurneringer.md)
     - [Administrasjon av turneringer og deltakere](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/administrasjon.md)
       - [Turnering](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admTurnering.md)
       - [Lag](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admLag.md)
       - [Spiller](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admSpiller.md)
   - [System Requirements](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/systemRequirements.md)


### Bilde:   
This is our [Welcome Screen]![programstart.png](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/Images/programStart.png)

### Bruk:
This is the menu and the user has four options, S, V, A and Q. If user types inn S, it allows you to play the tournament user has added. V is show tournaments. All tournaments that have been addded will show on the screen. A is administration of tournaments and players. Here the user can add tournaments and players, modify and delete. The last option is quit. If user types in Q the program will end.

 - S: Spill turnering
 - V: Vis Turneringsformat
 - A: Administrasjon av spillere, lag og turneringer.
 - Q: Returner

### Andre ting som skjer?
Ingenting.

