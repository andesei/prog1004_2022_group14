- [Product Manual](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/productManual.md)
   - [Welcome screen](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/welcomeScreen.md)
     - [Spill turnering](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/spillTurnering.md)
       - [Spill Neste match](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/spillTurneringNeste.md)
       - [Liste opp gjenværende matcher for runde](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/spillTurneringListeOpp.md)
       - [**se turneringsbracket**](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/spillTurneringSeBracket.md)


### Bilde:
![spillTurneringSeBracket.png](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/Images/spillTurneringSeBracket.png)

### Bruk:
User choose option T, show tournamentbracket. The tournamentbracket will be printed out. As you can see on the picture first all the players are listed, then the winners will be listed again. 

### Andre ting som skjer?
Ingenting.
