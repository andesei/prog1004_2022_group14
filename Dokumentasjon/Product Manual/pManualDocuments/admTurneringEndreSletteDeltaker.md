 - [Product Manual](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/productManual.md)
    - [Administrasjon av turneringer og deltakere](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/administrasjon.md)
       - [Turnering](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admTurnering.md)
          - [Ny turnering](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admTurneringNy.md)
          - [Vis alle turneringer](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admTurneringVis.md)
          - [Endre turnering](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admTurneringEndre.md)
             - [Legge til deltaker](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admTurneringEndreLeggTil.md)
             - [**Slette deltaker**](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admTurneringEndreSletteDeltaker.md)
             - [Vis alle deltakere](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admTurneringEndreVisDeltakere.md)
             - [Vis turneringsformat](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admTurneringEndreVisBracket.md)
             - [Endre turneringens navn](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admTurneringEndreNavn.md)
          - [Slette turnering](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admTurneringSlett.md)

### Bilde: 

![Slette deltaker fra turnering](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/admTurneringEndreSlettDeltaker.md)


### Bruk: 
Alle registrerte spillere listes opp etter GamerTag eller LagNavn (avhengig av turneringstype). Bruker bes så skrive inn lagnavn eller GamerTag for deltaker som ønskes slettet fra turneringen.
Sletting av deltakere avsluttes med 'Q', eller når turneringen er tom.

Tomme matchBrackets kan erstattes med ny spiller via 'Legge til deltaker" funksjonen.

### Andre ting som skjer?
peker til deltaker fjernes fra tournamentMatch vectoren tilhørende turneringstypen (Rounds, Qf, Sf eller Gf) 
