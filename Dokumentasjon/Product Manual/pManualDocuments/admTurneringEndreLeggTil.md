 - [Product Manual](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/productManual.md)
    - [Administrasjon av turneringer og deltakere](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/administrasjon.md)
       - [Turnering](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admTurnering.md)
          - [Ny turnering](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admTurneringNy.md)
          - [Vis alle turneringer](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admTurneringVis.md)
          - [Endre turnering](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admTurneringEndre.md)
             - [**Legge til deltaker**](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admTurneringEndreLeggTil.md)
             - [Slette deltaker](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admTurneringEndreSletteDeltaker.md)
             - [Vis alle deltakere](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admTurneringEndreVisDeltakere.md)
             - [Vis turneringsformat](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admTurneringEndreVisBracket.md)
             - [Endre turneringens navn](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admTurneringEndreNavn.md)
          - [Slette turnering](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admTurneringSlett.md)

### Bilde: 

![Legge til spiller i turnering.png](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/Images/admTurneringLeggTil.png)

### Bruk: 
Alle registrerte spillere listes opp etter GamerTag eller LagNavn (avhengig av turneringstype). Bruker bes så skrive inn lagnavn eller GamerTag for å registrere deltaker til turnering.
Innlesing av deltakere kan avsluttes med 'Q', ellers avsluttes innlesing automatisk når turneringen er full.

### Andre ting som skjer?
peker til deltaker legges inn i tournamentMatch vectoren tilhørende turneringstypen (Rounds, Qf, Sf eller Gf) 
