# Product Manual
This is the manual for TournamentPro - open source software that makes running your tournaments easier!

- [Product Manual](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/productManual.md)
   - [**System Requirements**](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/systemRequirements.md)

### System Requirements:
TBD
