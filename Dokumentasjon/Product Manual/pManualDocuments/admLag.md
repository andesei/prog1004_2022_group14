- [Product Manual](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/productManual.md)
  - [Administrasjon av turneringer og deltakere](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/administrasjon.md)
       - [**Lag**](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admLag.md)
          - [Nytt lag](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admLagNy.md)
          - [Vis alle lag](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admLagVis.md)
          - [Endre lag](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admLagEndre.md)
             - [Vis lagets spillere](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admLagEndreVisSpillere.md)
             - [Endre lagnavn](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admLagEndreNavn.md)
             - [Endre nasjonalitet](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admLagEndreNasjonalitet.md)
             - [Legge til spiller i lag](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admLagEndreLeggeTil.md)
             - [Slette spiller fra lag](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admLagEndreSlett.md)
          - [Slette lag](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/admLagSlett.md)

### Bilde:
![Administrer Lag](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/Images/admLag.png)

### Bruk:
Ber Bruker velge hva slags administrasjon han vil gjøre:

 - N: Nytt lag
 - V: Vis alle lag
 - E: Endre lag (dvs modifisere lagnavn/nasjonalitet, legge til eller slette spillere fra lag)
 - S: Slette lag
 - Q: Returner

### Andre ting som skjer?
Ingenting.
