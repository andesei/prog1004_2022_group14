- [Product Manual](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/productManual.md)
   - [Welcome screen](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/welcomeScreen.md)
     - [Spill turnering](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/spillTurnering.md)
       - [Spill Neste match](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/spillTurneringNeste.md)
       - [**Liste opp gjenværende matcher for runde**](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/spillTurneringListeOpp.md)
       - [se turneringsbracket](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/pManualDocuments/spillTurneringSeBracket.md)


### Bilde:
![spillTurneringListeOpp.png](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/Images/spillTurneringListeOpp.png)

### Bruk:
User choose option L, list of remaining matches for round. The remaining matches for round will be printed out, and is in this round between the players who played in round match 1 and round match 2. They will now play in semifinale match 1 and semifinale match 2. 

### Andre ting som skjer?
Ingenting.
