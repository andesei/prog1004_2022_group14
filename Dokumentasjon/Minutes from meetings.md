# Minutes from meetings - PROG1004 - Group14 (Latest first)


### Team meeting 20.04.2022

 - Recap of second client meeting
 - Better use of issues

 Priorities: 
 - Defining the functionality of the program in the Vision document and make issues for them
 - Plotting in lacking functionality in the issue board
 - Wiki - Sequence diagram and user manual.
 - Changing the button choices

Meeting took place for: 1 hour




### Second iteration TA meeting 31.03.2022

- Goals for coding the project
- Implementing the changes in the project 
- Gitlab issues for everything
- Describe every step of what we've done. 
  - Use the todo list
- Try to be done with the program itself by 15th of april. 

Meeting took place for: 1 hour




### Team meeting 16.03.2022
- What functions/features do we want?
    - Automatic stat tracking for both teams and individual players. 
- Which bulletpoints do we want to remove?
- Start using Issue Board!
- Go through Vision Document to know what to take up with the TA.

- Messaged TA about weekly meeting.

Meeting took place for: 1 hour




### First Client Meeting 15.03.2022

*** Vision document: **
- What can go wrong? Not enough users, someone gets sick
    - Next column: What would happen?
        - next: How we'll address it
##### Risk poker:
Probably of sickness - Everyone comes with own number
Better than just discussing it because of influence.

User study report: Describing our users

- Add gender, background, age etc.
- Do they have a background in programming?
- What did we ask them to do? 
- User X gave us this feedback, user Y gave us this feedback etc.
- Might have common feedback on something. --> Summary
- Improovements. eg. size should be increased. 
- After user tests, we can make wireframe v2.

- Make an MVP, make a couple of functions
- User feedback. Feedback on one function can work on others too.

Questions for TA: Walkthrough of the Vision document. 
Meeting took place for: 1 hour




### Product Owner meeting 14.03.2022

- Meeting with Jon (Product owner)
    - Showing him the prototype of the current product.
    - Discuss being able to add players and their dependencies.
        - Should players be able to have statistics independent of team?
            - Team, number of wins etc. One choice for all stats.
Meeting took place for: 1 hour




### Group meeting 14.03.2022

- Further planning on what to present tomorrow and what to deliver in the next iterateion.

Meeting took place for: 1 hour




### Meeting with TA 11.03.2022

- Got some feedback on what to do for second iteration (Wiki and MVP).
- We've also decided that Caroline and Anders start working on the wiki while Karl-Henrik, Nikolay and Patrik work on MVP.

Meeting took place for: 1 hour.




### Product Owner meeting 02.03.2022

Summary:
- Single elimination bracket tournament style
- 4 - 16 single player or multiple player teams. (+)
- Multi-platform. Non-dependent on game.
- User manual(++)
- Access control. Wants to be able to see match scores/ current standings in the tournament. (-)
    - Password for access to the program
    - Password for access to editing the program(?)
- Long term storage of results.
- Manual(+) / random (-) seeding. Wants to be able to change.
- Want to be able to pick walkover. (or add new teams during the tournament)(+)
- Questions for the QnA session the following day? 
#### '++' = must  '+' = high priority  '-' = low priority

Meeting took place for: 1 hour. 




### TA meeting 28.02.2022

Summary:
- Have a meeting with 'product owner/client'.
- Ideas/Vision Document
- Small intro plan what it should do and how it should work, goals, something unique(?).
- Then wireframe and Vision Document. 
- Weekly meetings with product owner(?)
- Vision Document: Fill out on the big bullet points 

Meeting took place for: 1 hour.




### Team meeting 25.02.2022 

- Started setting up our Gitlab repo, giving everyone rights and setting it up. 
- We've contacted the TA about our weekly mandatory meetings and sent him our preferred meeting times.
- Discussed briefly what we want our project to be about and agreed that we'd all ask around for a product owner/client.
- Installed Balsamiq Wireframes.
- Found meeting times which fit for all of us.

Meeting took place for: 
2,5 hours.
