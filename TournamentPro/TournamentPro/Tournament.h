#ifndef Tournament_h
#define Tournament_h

#include <vector>
#include <iostream>
#include "TournamentStruct.h"
#include "Funksjoner.h"

class Tournament {
    private:
    std::string trnmtName;
    bool trnmtTeamBased;
    int trnmtParticipants;
    std::vector <TournamentMatch*> tournamentRoundMatch;
    std::vector <TournamentMatch*> tournamentQfMatch;
    std::vector <TournamentMatch*> tournamentSfMatch;
    std::vector <TournamentMatch*> tournamentGfMatch;
    int roundMatchesPlayed,
        qfMatchesPlayed,
        sfMatchesPlayed,
        gfMatchesPlayed;
    
    const int roundMatchesTotal = 8, qfMatchesTotal = 4,
              sfMatchesTotal = 2, gfMatchesTotal = 1;
    
    public:
    Tournament();
    Tournament(std::ifstream &inputFile);
    ~Tournament();
    
    bool teamBased ();
    void writeTournament();
    void writeTournamentPlayer16();
    void writeTournamentPlayer8();
    void writeTournamentPlayer4();
    void writeTournamentTeam16();
    void writeTournamentTeam8();
    void writeTournamentTeam4();
    void constructorReadFromFile(std::ifstream &inputFile, std::vector <TournamentMatch*> bracketMatch);
    void addParticipantHelper();
    void addParticipant(std::vector <TournamentMatch*> bracketMatch);
    bool hasName(const std::string findTournament) const;
    void writeName();
    std::string returnName();
    bool returnTeamBased();
    int returnTrnmtParticipants();
    void writeModifyTournamentMenu();
    void modifyTournamentMenu();
    void modifyTournamentName();
    void writeTournamentToFile(std::ofstream &outputFile);
    void writeTournamentToFileHelper(std::ofstream &outputFile, std::vector <TournamentMatch*> bracketMatch);
    void writePlayTournamentMenu();
    void playTournamentMenu();
    void listRemainingMatches();
    void playNextTournamentMatch();
    bool playNextTournamentBracketMatch(std::vector <TournamentMatch*> currentBracketMatch, std::vector <TournamentMatch*> nextBracketMatch, int& bracketMatchesPlayed, int bracketMatchesTotal);
    bool tournamentMatchHasPlayerNullPtr(std::vector <TournamentMatch*> bracketMatchToCheck);
    bool tournamentMatchHasTeamNullPtr(std::vector <TournamentMatch*> bracketMatchToCheck);
    bool playNextTournamentGfMatch();
    void removeParticipantInput();
    void removeParticipantHelper(std::string participantToDelete);
    void removeParticipant(std::vector <TournamentMatch*> bracketMatch, std::string participant);
    void listAllParticipants();
    bool hasPlayerOrTeam(std::string participant);
    
};

Tournament* findTournament(const std::string findTournament);




#endif /* Tournament_h */
