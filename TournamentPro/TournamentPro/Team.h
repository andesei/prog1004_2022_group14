#ifndef Team_h
#define Team_h

#include <iostream>
#include <string>
#include "TeamStruct.h"


class Team {
    private:
        std::string teamName;
        std::string teamNationality;
        int tournamentWins;
        int tournamentsPlayed;
        int matchWins;
        int matchesPlayed;
        TeamMembers* teamMembers;
    
    public:
    Team ();
    Team(std::ifstream &inputFile);
    ~Team();
    void writeTeamToFile(std::ofstream &outputFile);
    void writeName();
    bool hasName (const std::string findTeam) const;
    void writeModifyTeamMenu();
    void modifyTeamMenu();
    void modifyTeamName();
    void modifyNationality();
    void addPlayer();
    void removePlayer();
    std::string returnName();
    void listTeamMembers();
    void plusMatchWins(bool grandFinal);
    void plusMatchesPlayed(bool knockout);
    void playerDeleted (std::string gamerTag);
    
    
};

Team* findTeam(const std::string findTeam);


#endif /* Team_h */
