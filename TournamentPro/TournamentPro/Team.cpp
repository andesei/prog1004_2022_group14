#include "Team.h"
#include "Funksjoner.h"

using namespace std;

extern vector <Team*> vTeam;
extern vector <Tournament*> vTournament;


/**
 * @brief empty constructor, initializes variables
 */
Team::Team() {
    // Empty constructor, initializes variables
    teamMembers = new TeamMembers;
    teamName = lesString("\nHva er lagnavnet?");
    teamNationality = lesString("\nHvilket land er laget fra?");
    cout << "\nLaget '" << teamName << "' er lagt til." << endl;
    
    matchesPlayed = 0;
    matchWins = 0;
    tournamentsPlayed = 0;
    tournamentWins = 0;
        
    
    char teamConstructorValg = lesChar("\nVil du legge til spillere nå? vilkårlig symbol eller 'N' for å gjøre det senere");
    
    if (teamConstructorValg != 'N') addPlayer();
}

/**
 * @brief leser inn lag, lagvariabler, finner peker til lagmedlemmer og legger inn dette.
 */
Team::Team(ifstream &inputFile) {
    int i = 0;
    string tempGamerTag;
    Player* vPlayer;
    teamMembers = new TeamMembers;
    
    inputFile >> teamMembers->memberAmount; inputFile.ignore();
    getline(inputFile, teamName, ';');
    getline(inputFile, teamNationality, ';'); inputFile.ignore(2, '\n');
    inputFile >> tournamentsPlayed; inputFile.ignore(1,';');
    inputFile >> tournamentWins; inputFile.ignore(1,';');
    inputFile >> matchesPlayed; inputFile.ignore(1,';');
    inputFile >> matchWins; inputFile.ignore(1,';'); inputFile.ignore(2,'\n');
    
    
    /* //Brukt for å teste filskriving/fillesing
    cout << endl << "Team " << teamName << " location tester" << endl;
    cout << teamMembers->memberAmount << " " << teamName << " " << teamNationality << endl;
    cout << tournamentsPlayed << " " << tournamentWins << " " << matchesPlayed << " " << matchWins << endl; */
    
    getline(inputFile, tempGamerTag, ';');
    
    if (tempGamerTag == "EndOfTeamMembers") inputFile.ignore(2, '\n');
    else while (tempGamerTag != "EndOfTeamMembers") {
        vPlayer = findPlayer(tempGamerTag);
        teamMembers->teamMember.push_back(vPlayer);
        if ((i+1)%4 == 0) inputFile.ignore(2, '\n');
        getline(inputFile, tempGamerTag, ';');
        if ( i == teamMembers->memberAmount || tempGamerTag == "EndOfTeamMembers") inputFile.ignore(2, '\n');
        i++;
    }
}


/**
 * @brief Destructor som sletter alle pekerne fra medlemsstruct og fjerner medlemskapet fra alle spillere.
 */
Team::~Team() {
    for (int i = 0; i<teamMembers->teamMember.size(); i++) {
        teamMembers->teamMember[i]->removeTeamMembership(teamName);
        teamMembers->teamMember.erase(teamMembers->teamMember.begin() +i);
        teamMembers->memberAmount--;
    }
    delete teamMembers;
    
    for (int i = 0; i<vTournament.size(); i++) {
        if (vTournament[i]->hasPlayerOrTeam(teamName) == true) {
            vTournament[i]->removeParticipantHelper(teamName);
        }
    }
}
    
/**
 * @brief Writes teamdata to file
 * @param outputFile file to be written to
 */
void Team::writeTeamToFile(ofstream &outputFile) {
    outputFile << teamMembers->teamMember.size() << ';' << teamName << ';' << teamNationality << ';' <<endl;
    outputFile << tournamentsPlayed << ';' << tournamentWins << ';' << matchesPlayed << ';' << matchWins << ';' << endl;
    
    if (teamMembers->teamMember.size() == 0) outputFile << "EndOfTeamMembers" << ';' << endl;
    else for (int i = 0; i<teamMembers->teamMember.size(); i++) {
        outputFile << teamMembers->teamMember[i]->returnGamerTag() << ';';
        if ((i+1)%4 == 0) outputFile << endl;
        if (i+1 == teamMembers->teamMember.size()) outputFile << "EndOfTeamMembers" << ';' << endl;
    }
    cout << "\t" <<teamName << " skrevet til fil." << endl;
}



/**
 * @brief Hjelpefunksjon til findTeam
 * @param findTeam Sjekker om lag har samme navn som param.
 */
bool Team::hasName (const string findTeam) const {
    if (teamName == findTeam) return true;
    else return false;
}


/**
 * @brief skriver lagnavnet
 */
void Team::writeName() {
    cout << teamName << endl;
}



/**
 * @brief Skriver meny for modifisering av laginformasjon
 */
void Team::writeModifyTeamMenu() {
    cout << "\nMenyvalg:" << endl;
    cout << "\tV: Vis lagets spillere" << endl;
    cout << "\tE: Endre lagnavn" << endl;
    cout << "\tN: Endre nasjonalitet" << endl;
    cout << "\tL: Legge til spiller i lag" << endl;
    cout << "\tS: Slette spiller fra lag" << endl;
    cout << "\tQ: Returner" << endl;
}

/**
 * @brief Meny for modifisering av lag
 */
void Team::modifyTeamMenu() {
    writeModifyTeamMenu();
    char kommando = lesChar("\nKommando");
    
    while (kommando != 'Q') {
        switch (kommando) {
            case 'V': listTeamMembers();                break;
            case 'E': modifyTeamName();                 break;
            case 'N': modifyNationality();              break;
            case 'L': addPlayer();                      break;
            case 'S': removePlayer();                   break;
            case 'Q': cout << "Returnerer..." << endl;  break;
            default:  writeModifyTeamMenu();            break;
        }
        writeModifyTeamMenu();
        kommando = lesChar("\nKommando");
    }
}

/**
 * @brief endrer lagets navn
 */
void Team::modifyTeamName() {
    teamName = lesString("\nSkriv inn nytt lagNavn: ");
    cout << "\nNytt lagnavn er: " << teamName << endl;
}

/**
 * @brief endrer lagets nasjonalitet
 */
void Team::modifyNationality() {
    teamNationality = lesString("\nSkriv inn ny nasjonalitet: ");
    cout << "\nNy nasjonalitet er: " << teamNationality << endl;
}


/**
 * @brief legger til spiller i laget. Legger også lagnavnet inn i spillerobjektet
 */
void Team::addPlayer() {
    string addPlayer;
    Player* vAddPlayer;
    listAllPlayers();
    
    do {
        addPlayer = lesString("\nSkriv inn gamertag til spiller som skal legges til, 'Q' for å avslutte innlegging av spillere");
        vAddPlayer = findPlayer(addPlayer);
        
        if (vAddPlayer != nullptr) {
            for (int i = 0; i<teamMembers->teamMember.size(); i++) {
                if (teamMembers->teamMember[i] == nullptr ) {
                    teamMembers->teamMember[i] = vAddPlayer;
                    findPlayer(addPlayer)->addTeamMembership(teamName);
                    teamMembers->memberAmount++; }
            }
            teamMembers->teamMember.push_back(vAddPlayer);
            findPlayer(addPlayer)->addTeamMembership(teamName);
            teamMembers->memberAmount++;
        } else if (addPlayer == "Q") cout << endl << "Returnerer..." << endl;
        else cout << endl << "Finner ikke gamertag..." << endl;
    } while (addPlayer != "Q");
}

/**
 * @brief lister lagets medlemmer
 */
void Team::listTeamMembers() {
    cout << "\nLister alle lagmedlemmer: " << endl;
    for (int i = 0; i<teamMembers->teamMember.size(); i++) {
        cout << "\t" << left << setw(20) << teamMembers->teamMember[i]->returnGamerTag();
        cout << right << "Spillernavn: " << left << setw(20) <<  teamMembers->teamMember[i]->returnName();
        cout << right << "Nasjonalitet: " << left << setw(20) << teamMembers->teamMember[i]->returnNationality() << endl;
    }
}


/**
 * @brief Fjerner spiller fra laget. Fjerner også lagnavnet fra spillerobjektet.
 */
void Team::removePlayer(){
    string removePlayer;
    Player* vRemovePlayer;
    listTeamMembers();
    
    do {
        removePlayer = lesString("\nSkriv inn gamertag til spiller som skal slettes, 'Q' for å avslutte");
        vRemovePlayer = findPlayer(removePlayer);
        
        if (vRemovePlayer != nullptr) {
            for (int i = 0; i<teamMembers->teamMember.size(); i++) {
                if (teamMembers->teamMember[i]->returnGamerTag() == removePlayer) {
                    teamMembers->teamMember[i]->removeTeamMembership(teamName);
                    teamMembers->teamMember.erase (teamMembers->teamMember.begin() +i);
                    teamMembers->memberAmount--;
                }
            }
        } else if (removePlayer == "Q") cout << endl << "Returnerer..." << endl;
        else cout << endl << "Finner ikke gamertag..." << endl;
    } while (removePlayer != "Q");
}


/**
 * @brief hjelpefunksjon som fjerner spiller fra lag hvis spilleren blir slettet
 * @param deletedPlayerTag gamertagen til spilleren som blir slettet.
 */
void Team::playerDeleted (string deletedPlayerTag) {
    for (int i = 0; i<teamMembers->teamMember.size(); i++) {
        if (teamMembers->teamMember[i]->returnGamerTag() == deletedPlayerTag) {
            teamMembers->teamMember.erase (teamMembers->teamMember.begin() +i);
        }
    }
}

/**
 * @brief returnerer lagets navn
 * @return lagnavn
 */
std::string Team::returnName() {
    return teamName;
}


/**
 * @brief Lag som vant runde. Øker matchWins og matchesPlayed med 1
 *            hvis param == true så økes også tournamentMatchWins og tournamentMatchesPlayed med 1.
 *            øker også disse variablene for medlemmer på laget.
 * @param grandFinal om matchen er en finale eller ikke
 */
void Team::plusMatchWins(bool grandFinal) {
    matchWins++;
    matchesPlayed++;
    if (grandFinal == true) {
        tournamentWins++;
        tournamentsPlayed++;
    }
    for (int i = 0; i<teamMembers->teamMember.size(); i++) {
        teamMembers->teamMember[i]->plusMatchWins(grandFinal);
    }
}


/**
 * @brief Lag som tapte runde. Øker matchesPlayed med 1
 *            øker tournamentsPlayed med 1 hvis det var knockoutrunde.
 *            øker også disse variablene for medlemmer på laget.
 * @param knockout om spilleren ble slått ut av turneringen
 */
void Team::plusMatchesPlayed(bool knockout) {
    matchesPlayed++;
    if (knockout == true) tournamentsPlayed++;
    
    for (int i = 0; i<teamMembers->teamMember.size(); i++) {
        teamMembers->teamMember[i]->plusMatchesPlayed(knockout);
    }
}
