#include <stdio.h>
#include "Player.h"
#include "Funksjoner.h"

using namespace std;
extern vector<Player*> vPlayer;
extern vector<Tournament*> vTournament;

/**
 * @brief Empty constructor, initializes variables
 */
Player::Player() {
    gamerTag = lesString("Hva er spillerens gamertag?");
    playerName = lesString("Hva heter spilleren");
    playerNationality = lesString("Hvor kommer spilleren fra?");
    cout << "\nSpilleren '" << gamerTag << "' er lagt til." << endl;
    tournamentsPlayed = 0;
    tournamentWins = 0;
    matchesPlayed = 0;
    matchWins = 0;
}


/**
 * @brief constructor som leser inn data.
 */
Player::Player(ifstream &inputFile) {
    int i = 0;
    string tempMembership;
    getline(inputFile, playerName, ';');
    getline(inputFile, gamerTag, ';');
    getline(inputFile, playerNationality, ';'); inputFile.ignore(2,'\n');
    inputFile >> tournamentsPlayed; inputFile.ignore(1,';');
    inputFile >> tournamentWins; inputFile.ignore(1,';');
    inputFile >> matchesPlayed; inputFile.ignore(1,';');
    inputFile >> matchWins; inputFile.ignore(1,';'); inputFile.ignore(2,'\n');
    
    getline(inputFile, tempMembership, ';');
    
    if (tempMembership == "EndOfTeamMembership") inputFile.ignore(2, '\n');
    else while (tempMembership != "EndOfTeamMembership") {
        teamMembership.push_back(tempMembership);
        if ((i+1)%4 == 0) inputFile.ignore(2, '\n');
        getline(inputFile, tempMembership, ';');
        if (tempMembership == "EndOfTeamMembership") inputFile.ignore(2, '\n');
        i++;
    }
    
}


/**
 * @brief destructor
 */
Player::~Player () {
    Team* tempTeam;
    for (int i = 0; i<teamMembership.size(); i++) {
        tempTeam = findTeam(teamMembership[i]);
        if (tempTeam != nullptr) {
            cout << "Sletter " << gamerTag << "fra " << tempTeam->returnName() << endl;
            tempTeam->playerDeleted(gamerTag);
        }
    }
    
    for (int i = 0; i<vTournament.size(); i++) {
        if (vTournament[i]->hasPlayerOrTeam(gamerTag) == true) {
            vTournament[i]->removeParticipantHelper(gamerTag);
        }
    }
}


/**
 * @brief constructor for å lage testbrukere (fjernes senere)
 */
Player::Player(int spillerNummer) {
    gamerTag = "Gamer "+to_string(spillerNummer);
    playerName = "Spiller "+to_string(spillerNummer);
    playerNationality = "Norge";
}


/**
 * @brief Skriver spillerens gamerTag
 */
void Player::writeGamerTag() {
    cout << gamerTag << endl;
}


/**
 * @brief Writes all playerdata
 */
void Player::writeAllData() {
    cout << left << endl << "\t" << setw(15) << gamerTag << right << setw(15) <<"Navn: ";
    cout << left << setw(20) << playerName;
    cout << right << setw(20) << "Nasjonalitet: " << left << playerNationality << endl;
    
    cout << "\t" << setw(5) << " " << setw(25) << right << "Matcher spilt: " << setw(3) << matchesPlayed << setw(17) << " "<< setw(20) << "Matcher vunnet: " << setw(3) << matchWins << endl;

    cout << "\t" << setw(5) << " " << setw(25) << "Turneringer spilt: " << setw(3) << tournamentsPlayed << setw(17) << " " <<setw(20) <<"Turneringer vunnet: " << setw(3) << tournamentWins << endl << endl;
}


/**
 * @brief Returnerer spillerens gamerTag
 */
string Player::returnGamerTag() {
    return gamerTag;
}

/**
 * @brief Skriver meny for modifisering av spillerinfo
 */
void Player::writeModifyPlayerMenu() {
    cout << "\nMenyvalg:" << endl;
    cout << "\tF: Endre spillers fødenavn" << endl;
    cout << "\tN: Endre spillers nasjonalitet" << endl;
    cout << "\tG: Endre GamerTag" << endl;
    cout << "\tQ: Avslutt" << endl;
}


/**
 * @brief Meny for modifisering av spiller
 */
void Player::modifyPlayerMenu() {
    writeModifyPlayerMenu();
    char kommando = lesChar("\nKommando");
    
    while (kommando != 'Q') {
        switch (kommando) {
            case 'F': modifyPlayerName();               break;
            case 'N': modifyPlayerNationality();        break;
            case 'G': modifyPlayerGamerTag();           break;
            case 'Q': cout << endl << "Returnerer...";  break;
            default:  writeModifyPlayerMenu();          break;
        }
        writeModifyPlayerMenu();
        kommando = lesChar("\nKommando");
    }
}


/**
 * @brief Endrer spillerens fødenavn
 */
void Player::modifyPlayerName() {
    cout << "\nRegistrert navn: " << playerName << endl;
    playerName = lesString("Angi nytt navn: ");
}


/**
 * @brief Endrer spillerens nasjonalitet
 */
void Player::modifyPlayerNationality() {
    cout << "\nRegistrert nasjonalitet: " << playerNationality << endl;
    playerNationality = lesString("Angi ny nasjonalitet: ");

}


/**
 * @brief Endrer spillerens gamertag
 */
void Player::modifyPlayerGamerTag() {
    cout << "\nRegistrert gamertag: " << gamerTag << endl;
    gamerTag = lesString("Angi ny gamertag: ");
}


/**
 * @brief Hjelpefunksjon til findPlayer
 * @param findTag Sjekker om spiller har samme gamertag som param.
 */
bool Player::hasGamerTag (const string findTag) const {
    if (gamerTag == findTag) return true;
    else return false;
}


/**
 * @brief Writes playerdata to file
 * @param outputFile file to be written to
 */
void Player::writePlayerToFile(ofstream &outputFile) {
    outputFile << playerName << ';' << gamerTag << ';' << playerNationality << ';' << endl;
    outputFile << tournamentsPlayed << ';' << tournamentWins << ';' << matchesPlayed << ';' << matchWins << ';' << endl;
    
    
    if (teamMembership.size() == 0) outputFile << "EndOfTeamMembership" << ';' << endl;
    else for (int i = 0; i<teamMembership.size(); i++) {
        outputFile << teamMembership[i] << ';';
        if ((i+1)%4 == 0) outputFile << endl;
        if (i+1 == teamMembership.size()) outputFile << "EndOfTeamMembership" << ';' << endl;
    }
    cout << "\t" <<gamerTag << " skrevet til fil." << endl;
    
}


/**
 * @brief legger til param strengen som et lagmedlemskap.
 * @param teamName laget spilleren får medlemskap i.
 */
void Player::addTeamMembership (const string teamName) {
    teamMembership.push_back(teamName);
}

/**
 * @brief laget param fjernes fra spillerens lagmedlemskap.
 * @param teamName laget spilleren ikke er medlem i lenger.
 */
void Player::removeTeamMembership (const string teamName) {
    
    for (int i = 0; i<teamMembership.size(); i++) {
        if (teamName == teamMembership[i]) {
            cout << "\nFjernet " << gamerTag << "'s medlemskap i "<< teamMembership[i];
            teamMembership.erase(teamMembership.begin()+i);
        }
    }
}


/**
 * @brief Returnerer spillerens navn
 */
string Player::returnName() {
   return playerName;
}


/**
 * @brief Returnerer spillerens gamerTag
 */
string Player::returnNationality() {
    return playerNationality;
}


/**
 * @brief Spiller som vant runde. Øker matchWins og matchesPlayed med 1
 *                  hvis param == true så økes også tournamentMatchWins og tournamentMatchesPlayed med 1
 * @param grandFinal om matchen er en finale eller ikke
 */
void Player::plusMatchWins(bool grandFinal) {
    matchWins++;
    matchesPlayed++;
    if (grandFinal == true) {
        tournamentWins++;
        tournamentsPlayed++;
    }
}


/**
 * @brief spiller som tapte runde. Øker matchesPlayed med 1
 *                  øker tournamentsPlayed med 1 hvis det var knockoutrunde
 * @param knockout om spilleren ble slått ut av turneringen
 */
void Player::plusMatchesPlayed(bool knockout) {
    matchesPlayed++;
    if (knockout == true) tournamentsPlayed++;
}


/**
 * @brief sletter medlemskapet hos alle lag:
 */
void Player::deleteAllTeamMemberships() {
    Team* tempTeam;
    for (int i = 0; i<teamMembership.size(); i++) {
        tempTeam = findTeam(teamMembership[i]);
        if (tempTeam != nullptr) {
            cout << endl << "Sletter " << gamerTag << "fra " << tempTeam->returnName() << endl;
            tempTeam->playerDeleted(gamerTag);
        }
    }
}
