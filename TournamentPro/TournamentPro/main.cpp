/**
 * main.cpp
 *  TournamentPro - An open source tournament management software.
 *
 * @author Anders Eiken
 * @author Patrik Andre Olaussen
 * @author Nikolay Savchuk
 * @author Caroline Bjerkaas
 * @author Karl -Henrik Horve
*/

#include <vector>
#include "Player.h"
#include "Tournament.h"
#include "Team.h"
#include "LesData2.h"

using namespace std;

vector <Team*> vTeam;                       // vector med peker til lagene
vector <Player*> vPlayer;                   // vector med peker til spillerne
vector <Tournament*> vTournament;           // vector med peker til turneringene


/* Hovedprogrammet */
int main() {
    readFromFile();
    
    /*
    for (int i = 0; i<16; i++) {
        vPlayer.push_back(new Player(i));
    }
     */
    
    char kommando;
  
    skrivMeny();
    kommando = lesChar("\nKommando");
    
    while (kommando != 'Q') {
        switch (kommando) {
            case 'S': playTournament();                            break;
            case 'V': listAllTournamentsDetailed();                break;
            case 'A': administrasjonsMeny();                       break;
            case 'Q': cout << "\nAvslutter og skriver til fil..."; break;
            default:  skrivMeny();                                 break;
        }
        skrivMeny();
        kommando = lesChar("\nKommando");
    }
    
    writeToFile();
    return 0;
}
