#ifndef Player_h
#define Player_h

#include <string>
#include <fstream>
#include <vector>
#include <iostream>

class Player{
    private:
    std::string playerName;                         // Spillerens navn
    std::string playerNationality;                  // Spillerens nasjonalitet
    std::string gamerTag;                           // Spillerens Gamer Tag
    std::vector <std::string> teamMembership;
    int tournamentWins;
    int tournamentsPlayed;
    int matchWins;
    int matchesPlayed;
    
    
    public:
    Player();
    Player(std::ifstream &inputFile);
    Player(int spillerNummer);
    ~Player();
    
    void writeAllData();
    void writeGamerTag();
    bool hasGamerTag (const std::string findTag) const;
    void modifyPlayerMenu();
    void writeModifyPlayerMenu();
    void modifyPlayerName();
    void modifyPlayerNationality();
    void modifyPlayerGamerTag();
    void writePlayerToFile(std::ofstream &outputFile);
    std::string returnGamerTag();
    std::string returnName();
    std::string returnNationality();
    void addTeamMembership (const std::string teamName);
    void removeTeamMembership (const std::string teamName);
    void plusMatchWins(bool grandFinal);
    void plusMatchesPlayed(bool knockout);
    void writeToFile(std::ofstream &outputFile);
    void deleteAllTeamMemberships();
};

Player* findPlayer(const std::string findTag);

#endif
