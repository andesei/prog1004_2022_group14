#ifndef Funksjoner_h
#define Funksjoner_h

#include <stdio.h>
#include <vector>
#include <iostream>
#include "LesData2.h"
#include "Player.h"
#include "Tournament.h"
#include "Team.h"


void skrivMeny();
void skrivAdministrasjonsMeny();
void administrasjonsMeny();
std::string lesString(const char* t);
void skrivAdministrasjonsMenyTurnering();
void administrasjonsMenyTurnering();
void skrivAdmMenuTeam();
void admMenuTeam();
void modifyTournament();
void deleteTournament();
void skrivAdministrasjonsMenySpiller();
void administrasjonsMenySpiller();
void nySpiller();
void listAllPlayers();
void listAllPlayersDetailed();
void selectPlayerToModify();
void deletePlayer();
void deleteTeam();
void modifyTeam();
void listAllTeams();
void listAllTournaments();
void listAllTournamentsDetailed();
void playTournament();
void writeToFile();
void readFromFile();


#endif
