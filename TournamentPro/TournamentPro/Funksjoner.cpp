#include "Funksjoner.h"
using namespace std;
extern vector <Team*> vTeam;
extern vector <Tournament*> vTournament;
extern vector <Player*> vPlayer;


void skrivMeny() {
    cout << "\nMenyvalg:" << endl;
    cout << "\tS: Spill turnering" << endl;
    cout << "\tV: Vis turneringer" << endl;
    cout << "\tA: Administrasjon av turneringer og deltakere" << endl;
    cout << "\tQ: Avslutt" << endl;
};

/*
 * Skriver administrasjonsmeny
 */
void skrivAdministrasjonsMeny() {
    cout << "\nHva vil du lage, endre eller slette?:" << endl;
    cout << "\tT: Turnering" << endl;
    cout << "\tL: Lag" << endl;
    cout << "\tS: Spiller" << endl;
    cout << "\tQ: Returner" << endl;
}

/*
 * Administrasjonsmenyen
 */
void administrasjonsMeny() {
    skrivAdministrasjonsMeny();
    char kommando = lesChar("\nKommando");
    
    while (kommando != 'Q') {
        switch (kommando) {
            case 'T': administrasjonsMenyTurnering();         break;
            case 'L': admMenuTeam();                          break;
            case 'S': administrasjonsMenySpiller();           break;
            case 'Q': cout << "\nReturnerer..." << endl;      break;
            default:  skrivAdministrasjonsMeny();             break;
        }
        skrivAdministrasjonsMeny();
        kommando = lesChar("\nKommando");
    }
}

/**
 * @brief Reads and returns a string.
 */
std::string lesString(const char* t)  {
    string tekst;
    bool valid = false;
    std::cout << t << ":  ";
    getline(cin, tekst);
    do {
        if (tekst.find(';') != std::string::npos) {
            cout << endl << "Ugyldig symbol ';' funnet, prøv på nytt:  ";
            getline(cin, tekst);
        } else if (tekst.find("EndOfTeamMembers") != std::string::npos) {
            cout << endl << "Ugyldig tekst 'EndOfTeamMembers' funnet, prøv på nytt:  ";
            getline(cin, tekst);
        } else valid = true;
    } while (valid == false);
    
  //  if (tekst == "q") std::transform(tekst.begin(), tekst.end(),tekst.begin(), ::toupper);
    return (tekst);
}


/*
 * SKriver Administrasjonsmenyen for lag
 */
void skrivAdmMenuTeam() {
    cout << "\nHva vil du gjøre?" << endl;
    cout << "\tN: Nytt lag" << endl;
    cout << "\tV: Vis alle lag" << endl;
    cout << "\tE: Endre lag" << endl;
    cout << "\tS: Slette lag" << endl;
    cout << "\tQ: Returner" << endl;
}


/*
 * Administrasjonsmeny for Lag
 * A - Lager nytt lag, constructor soerger for at laget får navn og nasjonalitet
 */
void admMenuTeam() {
    skrivAdmMenuTeam();
    char kommando = lesChar("\nKommando");

    while (kommando != 'Q') {
        switch (kommando) {
            case 'N': vTeam.push_back(new Team);          break;
            case 'V': listAllTeams();                     break;
            case 'E': modifyTeam();                       break;
            case 'S': deleteTeam();                       break;
            case 'Q': cout << "\nReturnerer..." << endl;  break;
            default:  skrivAdmMenuTeam();                 break;
        }
        skrivAdmMenuTeam();
        kommando = lesChar("\nKommando");
    }
}


/*
 * SKriver Administrasjonsmenyen for turnering
 */
void skrivAdministrasjonsMenyTurnering() {
    cout << "\nHva vil du gjøre?" << endl;
    cout << "\tN: Ny turnering" << endl;
    cout << "\tV: Vis alle turneringer" << endl;
    cout << "\tE: Endre turnering" << endl;
    cout << "\tS: Slette turnering" << endl;
    cout << "\tQ: Returner" << endl;
}


/*
 * Administrasjonsmeny for Turnering
 */
void administrasjonsMenyTurnering() {
    skrivAdministrasjonsMenyTurnering();
    char kommando = lesChar("\nKommando");

    while (kommando != 'Q') {
        switch (kommando) {
            case 'N': vTournament.push_back(new Tournament);   break;
            case 'V': listAllTournaments();                    break;
            case 'E': modifyTournament();                      break;
            case 'S': deleteTournament();                      break;
            case 'Q': cout << "\nReturnerer...";               break;
            default:  skrivAdministrasjonsMenyTurnering();     break;
        }
        skrivAdministrasjonsMenyTurnering();
        kommando = lesChar("\nKommando");
    }
}

/*
 * Velger turnering man vil gjøre endringer i
 */
void modifyTournament() {
    string modifyTournament;
    Tournament* vModifyTournament;
    listAllTournaments();
    
    do {
        modifyTournament = lesString("\nSkriv inn navn på turnering du vil gjøre endringer i, 'Q' for å avslutte");
        vModifyTournament = findTournament(modifyTournament);
        
        if (vModifyTournament != nullptr) {
            vModifyTournament->modifyTournamentMenu();
            break;
        } else if (modifyTournament == "Q") cout << endl << "Returnerer..." << endl;
        else cout << endl << "Finner ikke turneringsnavn..." << endl;
    } while (modifyTournament != "Q");
}


/**
 * @brief Sletter angitt spiller
 */
void deletePlayer() {
    listAllPlayers();
    string playerToDelete;
    Player* vPlayerToDelete;
    
    do {
        playerToDelete = lesString("\nSkriv inn gamertag til spiller som skal slettes, 'Q' for å avslutte");
        vPlayerToDelete = findPlayer(playerToDelete);
        
        if (vPlayerToDelete != nullptr) {
            char playerDeleteConfirmation = lesChar("Skriv inn 'J' for å bekrefte sletting");
            
            if (playerDeleteConfirmation == 'J') {
                for (int i = 0; i<vPlayer.size(); i++) {
                    if (vPlayer[i] == vPlayerToDelete) {
                        delete vPlayer[i];
                        vPlayer.erase(vPlayer.begin() +i);
                        break;
                    }
                }
            }
            
            else {
                cout << "Avbryter sletting. Returnerer..." << endl;
                break;
            }
            
        } else if (playerToDelete == "Q") cout << endl << "Returnerer..." << endl;
        else cout << endl << "Finner ikke gamertag..." << endl;
    } while (playerToDelete != "Q");
}

/**
 * @brief Sletter angitt turnering
 */
void deleteTournament() {
    string tournamentToDelete;
    Tournament* vTournamentToDelete;
    listAllTournaments();
    
    do {
        tournamentToDelete = lesString("\nSkriv inn navn på turneringen som skal slettes, 'Q' for å avslutte");
        vTournamentToDelete = findTournament(tournamentToDelete);
        
        if (vTournamentToDelete != nullptr) {
            char tournamentDeleteConfirmation = lesChar("Skriv inn J for å bekrefte sletting");
            
            if (tournamentDeleteConfirmation == 'J') {
                for (int i = 0; i<vTournament.size(); i++) {
                    if (vTournament[i] == vTournamentToDelete) {
                        delete vTournament[i];
                        vTournament.erase(vTournament.begin() +i);
                        break;
                    }
                }
            }
            
            else {
                cout << "Avbryter sletting. Returnerer..." << endl;
                break; }
            
        } else if (tournamentToDelete == "Q") cout << endl << "Returnerer..." << endl;
        else cout << endl << "Finner ikke turneringsnavn..." << endl;
    } while (tournamentToDelete != "Q");
}


/**
 * @brief Sletter angitt lag og medlemskap hos medlemsspillere
 */
void deleteTeam() {
    string teamToDelete;
    Team* vTeamToDelete;
    listAllTeams();
    
    do {
        teamToDelete = lesString("\nSkriv inn lagnavn til lag som skal slettes, 'Q' for å avslutte");
        vTeamToDelete = findTeam(teamToDelete);
        
        if (vTeamToDelete != nullptr) {
            char teamDeleteConfirmation = lesChar("Skriv inn J for å bekrefte sletting");
            
            if (teamDeleteConfirmation == 'J') {
                for (int i = 0; i<vTeam.size(); i++) {
                    if (vTeam[i] == vTeamToDelete) {
                        delete vTeam[i];
                        vTeam.erase(vTeam.begin() +i);
                        break;
                    }
                }
            }
            
            else {
                cout << "Avbryter sletting. Returnerer..." << endl;
                break; }
        } else if (teamToDelete == "Q") cout << endl << "Returnerer..." << endl;
        else cout << endl << "Finner ikke lagnavn..." << endl;
    } while (teamToDelete != "Q");
}


/*
 * SKriver Administrasjonsmenyen for spiller
 *
 */
void skrivAdministrasjonsMenySpiller() {
    cout << "\n Hva vil du gjøre?" << endl;
    cout << "\tN: Ny spiller" << endl;
    cout << "\tV: Vis alle spillere" << endl;
    cout << "\tD: Vis alle spillere med Detaljer" << endl;
    cout << "\tE: Endre spillerdetaljer" << endl;
    cout << "\tS: Slette spiller" << endl;
    cout << "\tQ: Returner" << endl;
}



/*
 * Administrasjonsmeny for spiller
 */
void administrasjonsMenySpiller() {
    skrivAdministrasjonsMenySpiller();
    char kommando = lesChar("\nKommando");

    while (kommando != 'Q') {
        switch (kommando) {
            case 'V': listAllPlayers();                     break;
            case 'D': listAllPlayersDetailed();             break;
            case 'N': vPlayer.push_back(new Player);        break;
            case 'E': selectPlayerToModify();               break;
            case 'S': deletePlayer();                       break;
            case 'Q': cout << "\nReturnerer...";            break;
            default:  skrivAdministrasjonsMenySpiller();    break;
        }
        skrivAdministrasjonsMenySpiller();
        kommando = lesChar("\nKommando");
    }
}


/*
 * Lister aller spillere, ber så bruker velge en spiller å gjøre endringer på.
 * Sender så bruker inn i menyen til spilleren
 */
void selectPlayerToModify() {
    string modifyPlayer;
    Player* vModifyPlayer;
    listAllPlayers();
    
    do {
        modifyPlayer = lesString("\nSkriv inn gamertag til spiller som skal modifiseres, 'Q' for å avslutte");
        vModifyPlayer = findPlayer(modifyPlayer);
        
        if (vModifyPlayer != nullptr) {
            vModifyPlayer->modifyPlayerMenu();
            break;
        } else if (modifyPlayer == "Q") cout << endl << "Returnerer..." << endl;
        else cout << endl << "Finner gamertag..." << endl;
    } while (modifyPlayer != "Q");
}


/**
 * @brief Lister alle spillerer etter gamertag
 *        kaller hjelpefunksjonen writeGamerTag for å skrive navnet.
 */
void listAllPlayers() {
    for (int i = 0; i<vPlayer.size(); i++) {
        cout << "\t";
        vPlayer[i]->writeGamerTag();
    }
}

/**
 * @brief Lister alle spillerer etter gamertag
 *        kaller hjelpefunksjonen writeGamerTag for å skrive navnet.
 */
void listAllPlayersDetailed() {
    for (int i = 0; i<vPlayer.size(); i++) vPlayer[i]->writeAllData();
}

/**
 * @brief lister opp alle lag.
 */
void listAllTeams() {
    for (int i = 0; i<vTeam.size(); i++) {
        cout << "\t";
        vTeam[i]->writeName();
    }
}

/**
 * @brief Velger lag man vil gjøre endringer i
 */
void modifyTeam() {
    string modifyTeam;
    Team* vModifyTeam;
    listAllTeams();
    do {
        modifyTeam = lesString("\nSkriv inn navn på laget du vil gjøre endringer i, 'Q' for å avslutte");
        vModifyTeam = findTeam(modifyTeam);
        if (vModifyTeam != nullptr) {
            vModifyTeam->modifyTeamMenu();
            break;
        } else if (modifyTeam == "Q") cout << endl << "Returnerer..." << endl;
        else cout << endl << "Finner ikke innskrevet lag..." << endl;
    } while (modifyTeam != "Q");

    
}


/**
 * @brief Finner turnering og returnerer om mulig en peker til turneringen
 * @param findTournament turneringen som skal finnes
 */
Tournament* findTournament(const string findTournament) {
    for (int i = 0; i<vTournament.size(); i++) {
        if (vTournament[i]->hasName(findTournament) == true) return vTournament[i];
    }
    return nullptr;
}


/**
 * @brief Finner spiller og returnerer om mulig en peker til spilleren
 * @param findTag gamertagen som skal finnes
 */
Player* findPlayer(const string findTag) {
    for (int i = 0; i<vPlayer.size(); i++) {
        if (vPlayer[i]->hasGamerTag(findTag) == true) return vPlayer[i];
    }
    return nullptr;
}


/**
 * @brief Finner lag og returnerer om mulig en peker til laget
 * @param findTeam lagnavn det letes etter
 */
Team* findTeam(const string findTeam) {
    for (int i = 0; i<vTeam.size(); i++) {
        if (vTeam[i]->hasName(findTeam) == true) return vTeam[i];
    }
    return nullptr;
}


/**
 * @brief lister alle turneringer
 */
void listAllTournaments() {
    if (vTournament.size() != 0) {
        for (int i = 0; i<vTournament.size(); i++) {
            cout << "\t";
            vTournament[i]->writeName();
        }
    } else cout << endl << "Ingen turneringer funnet." << endl;
}


/**
 * @brief lister alle turneringer med detaljer
 */
void listAllTournamentsDetailed() {
    if (vTournament.size() != 0) {
        cout << "\nSkriver ut alle turneringer med detaljer: " << endl;
        
        for (int i = 0; i<vTournament.size(); i++) {
            cout << "\t" << setw(20)<<vTournament[i]->returnName() << "\t- ";
            if (vTournament[i]->returnTeamBased() == true) {
                cout << "Lagbasert turnering med ";
            } else cout << "Enkeltspiller turnering med ";
            cout << vTournament[i]->returnTrnmtParticipants() << " deltakende spillere." << endl;
        }
    } else cout << endl << "Ingen turneringer funnet." << endl;
}


/**
 * @brief Ber bruker velge hvilken turnering som skal spilles.
 */
void playTournament() {
    listAllTournamentsDetailed();
    if (vTournament.size() != 0) {
        string tournamentToPlay = lesString("\nHvilken turnering skal spilles?");
        Tournament* vPlayTournament = findTournament(tournamentToPlay);
        
        if (vPlayTournament != nullptr) {
            vPlayTournament->playTournamentMenu(); }
        else cout << "\nFinner ikke turnering. Returnerer";
    }
}


/**
 * @brief Skriver all data til filer
 */
void writeToFile() {
    ofstream outputFile;
    
    outputFile.open("./PLAYERDATA.DTA");
    if (!outputFile) cout << "\nUtfil for spiller ikke funnet" << endl;
    outputFile << vPlayer.size() << endl;
    
    cout << "\nSkriver spillere til fil: " << endl;
    for (int i = 0; i<vPlayer.size(); i++) {
        vPlayer[i]->writePlayerToFile(outputFile);
    }
    outputFile.close();
    
    
    outputFile.open("./TEAMDATA.DTA");
    if (!outputFile) cout << "\nUtfil for lag ikke funnet" << endl;
        outputFile << vTeam.size() << endl;
    
    cout << "\nSkriver lag til fil: " << endl;
    for (int i = 0; i<vTeam.size(); i++) {
        vTeam[i]->writeTeamToFile(outputFile);
    }
    outputFile.close();
    
    outputFile.open("./TOURNAMENTDATA.DTA");
    if (!outputFile) cout << "\nUtfil for turnering ikke funnet" << endl;
    outputFile << vTournament.size() << endl;
    
    cout << "\nSkriver turneringer til fil: " << endl;
    for (int i = 0; i<vTournament.size(); i++) {
        vTournament[i]->writeTournamentToFile(outputFile);
    }
    outputFile.close();
}

/**
 * @brief Leser inn all data fra filer
 */
void readFromFile() {
    int nrOfPlayers, nrOfTeams, nrOfTournaments;
    ifstream inputFile;
    
    
    inputFile.open("PLAYERDATA.DTA");
    if (!inputFile) cout << "\nInnfil for spiller ikke funnet" << endl;
    else if (inputFile.peek() != std::ifstream::traits_type::eof()) {
        inputFile >> nrOfPlayers; inputFile.ignore(2,'\n');
        for (int i = 0; i<nrOfPlayers; i++) {
            vPlayer.push_back(new Player(inputFile));
        }
        cout << endl << "Spillere lest fra fil..." << endl;
        inputFile.close();
    }

    
    inputFile.open("TEAMDATA.DTA");
    if (!inputFile) cout << "\nInnfil for lag ikke funnet" << endl;
    else if (inputFile.peek() != std::ifstream::traits_type::eof()) {
        inputFile >> nrOfTeams; inputFile.ignore(2,'\n');
        for (int i = 0; i<nrOfTeams; i++) {
            
            vTeam.push_back(new Team(inputFile));
        }
        cout << "Lag lest fra fil..." << endl;
        inputFile.close();
    }
    
    
    inputFile.open("TOURNAMENTDATA.DTA");
    if (!inputFile) cout << "\nInnfil for turnering ikke funnet" << endl;
    else if (inputFile.peek() != std::ifstream::traits_type::eof()) {
        inputFile >> nrOfTournaments; inputFile.ignore(2, '\n');
        for (int i = 0; i<nrOfTournaments; i++) {
            vTournament.push_back(new Tournament(inputFile));
        }
        cout << "Turneringer lest fra fil..." << endl;
        inputFile.close();
    }
}
