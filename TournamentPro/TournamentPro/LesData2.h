/**
 *   Enkel verkt�ykasse for � lese:  tegn og tall.
 *
 *   @file     LesData2.H
 *   @author   Frode Haug, NTNU
 */


#ifndef __LESDATA2_H
#define __LESDATA2_H


#include <iostream>            //  cin, cout
#include <iomanip>             //  setprecision
#include <cctype>              //  toupper
#include <cstdlib>             //  atoi, atof

const int  MAXCHAR = 200;      //  Max.tegn i input-buffer.

char  lesChar(const char* t);
float lesFloat(const char* t, const float min, const float max);
int   lesInt(const char* t, const int min, const int max);


#endif
