#include <stdio.h>
#include "Tournament.h"

using namespace std;
extern vector <Tournament*> vTournament;

/**
 * @brief constructor for initializing tournament objects
 */
Tournament::Tournament() {
    trnmtName = lesString("\nHva heter turneringen?");
    trnmtTeamBased = teamBased();
    trnmtParticipants = lesInt("\nHvor mange deltakere er det i turneringen? 4, 8 eller 16:", 4, 16);
    while (trnmtParticipants != 4 && trnmtParticipants != 8 && trnmtParticipants != 16) {
        trnmtParticipants = lesInt("\nHvor mange deltakere er det i turneringen? 4, 8 eller 16:", 4, 16);
    }
    
    if (trnmtParticipants == 16) {
        roundMatchesPlayed = 0;
        qfMatchesPlayed = 0;
        sfMatchesPlayed = 0;
        gfMatchesPlayed = 0;
    }
    
    //Genererer objekter i strukten til turnerings første runde ved 8 deltakere
    else if (trnmtParticipants == 8) {
        roundMatchesPlayed = 8;
        qfMatchesPlayed = 0;
        sfMatchesPlayed = 0;
        gfMatchesPlayed = 0;
    }
    
    //Genererer objekter i strukten til turnerings første runde ved 4 deltakere
    else if (trnmtParticipants == 4) {
        roundMatchesPlayed = 8;
        qfMatchesPlayed = 4;
        sfMatchesPlayed = 0;
        gfMatchesPlayed = 0;
    }
    
    for (int i = 0; i<8; i++) {
        tournamentRoundMatch.push_back(new TournamentMatch);
        if ((i+1)%2 == 0) {
            tournamentQfMatch.push_back(new TournamentMatch);
        }
        if ((i+1)%4 == 0) tournamentSfMatch.push_back(new TournamentMatch);
        if ((i+1)%8 == 0) tournamentGfMatch.push_back(new TournamentMatch);
    }
    
    for (int i = 0; i<tournamentRoundMatch.size(); i++) {
        tournamentRoundMatch[i]->player1 = nullptr;
        tournamentRoundMatch[i]->player2 = nullptr;
        tournamentRoundMatch[i]->team1 = nullptr;
        tournamentRoundMatch[i]->team2 = nullptr;
        tournamentRoundMatch[i]->matchPlayed = false;
        
        if ((i+1)%2 == 0) {
            tournamentQfMatch[i/2]->player1 = nullptr;
            tournamentQfMatch[i/2]->player2 = nullptr;
            tournamentQfMatch[i/2]->team1 = nullptr;
            tournamentQfMatch[i/2]->team2 = nullptr;
            tournamentQfMatch[i/2]->matchPlayed = false;
        }
        
        if ((i+1)%4 == 0) {
            tournamentSfMatch[i/4]->player1 = nullptr;
            tournamentSfMatch[i/4]->player2 = nullptr;
            tournamentSfMatch[i/4]->team1 = nullptr;
            tournamentSfMatch[i/4]->team2 = nullptr;
            tournamentSfMatch[i/4]->matchPlayed = false;
        }
        
        if ((i+1)%8 == 0) {
            tournamentSfMatch[i/8]->player1 = nullptr;
            tournamentSfMatch[i/8]->player2 = nullptr;
            tournamentSfMatch[i/8]->team1 = nullptr;
            tournamentSfMatch[i/8]->team2 = nullptr;
            tournamentSfMatch[i/8]->matchPlayed = false;
        }
    }
}


/**
 * @brief Constructor for å lese fra fil.
 * @param inputFile filen det leses fra
 */
Tournament::Tournament(ifstream &inputFile) {
    getline(inputFile, trnmtName, ';');
    inputFile >> trnmtTeamBased; inputFile.ignore(1,';');
    inputFile >> trnmtParticipants; inputFile.ignore(1,';'); inputFile.ignore(2,'\n');
    inputFile >> roundMatchesPlayed; inputFile.ignore(1,';');
    inputFile >> qfMatchesPlayed; inputFile.ignore(1,';');
    inputFile >> sfMatchesPlayed; inputFile.ignore(1,';');
    inputFile >> gfMatchesPlayed; inputFile.ignore(1,';'); inputFile.ignore(2,'\n');
    
    for (int i = 1; i<=8; i++) {
        tournamentRoundMatch.push_back(new TournamentMatch);
        if (i%2 == 0) tournamentQfMatch.push_back(new TournamentMatch);
        if (i%4 == 0) tournamentSfMatch.push_back(new TournamentMatch);
        if (i%8 == 0) tournamentGfMatch.push_back(new TournamentMatch);
    }
        
    constructorReadFromFile(inputFile, tournamentRoundMatch);
    constructorReadFromFile(inputFile, tournamentQfMatch);
    constructorReadFromFile(inputFile, tournamentSfMatch);
    constructorReadFromFile(inputFile, tournamentGfMatch);
    
}


/**
 * @brief tournament destructor
 */
Tournament::~Tournament () {
    for (int i = 0; i<tournamentRoundMatch.size(); i++) {
        delete tournamentRoundMatch[i];
    }
    for (int i = 0; i<tournamentQfMatch.size(); i++) {
        delete tournamentQfMatch[i];
    }
    for (int i = 0; i<tournamentSfMatch.size(); i++) {
        delete tournamentSfMatch[i];
    }
    for (int i = 0; i<tournamentGfMatch.size(); i++) {
        delete tournamentGfMatch[i];
    }
}


/**
 * @brief Skriver meny for modifisering av spillerinfo
 */
void Tournament::writeModifyTournamentMenu() {
    cout << "\nMenyvalg:" << endl;
    cout << "\tL: Legge til deltaker" << endl;
    cout << "\tS: Slette deltaker" << endl;
    cout << "\tD: Vis alle deltakere" << endl;
    cout << "\tV: Vis turneringsformat" << endl;
    cout << "\tE: Endre Turneringens navn" << endl;
    cout << "\tQ: Avslutt" << endl;
}


/**
 * @brief Meny for modifisering av spiller
 */
void Tournament::modifyTournamentMenu() {
    writeModifyTournamentMenu();
    char kommando = lesChar("\nKommando");
    
    while (kommando != 'Q') {
        switch (kommando) {
            case 'E': modifyTournamentName();               break;
            case 'L': addParticipantHelper();               break;
            case 'S': removeParticipantInput();            break;
            case 'V': writeTournament();                    break;
            case 'D': listAllParticipants();                break;
            case 'Q': cout << "\nReturnerer" << endl;       break;
            default:  writeModifyTournamentMenu();          break;
        }
        writeModifyTournamentMenu();
        kommando = lesChar("\nKommando");
    }
}


/**
 * @brief Endrer navn på turneringen
 */
void Tournament::modifyTournamentName() {
    trnmtName = lesString("\nSkriv inn nytt navn på turneringen");
}


/**
 * @brief Velger om turneringen er for individuelle spillere eller lag
 */
bool Tournament::teamBased() {
    int valg = lesInt("\nEr turneringen lagbasert? 1 for ja, 0 for nei", 0, 1);
    if (valg == 1) return true;
    else return false;
}

/**
 * @brief Velger om om det skal skrives ut turneringsformat for spiller eller lag
 */
void Tournament::writeTournament() {
    if (trnmtTeamBased == true && trnmtParticipants == 16) writeTournamentTeam16();
    else if (trnmtTeamBased == true && trnmtParticipants == 8) writeTournamentTeam8();
    else if (trnmtTeamBased == true && trnmtParticipants == 4) writeTournamentTeam4();
    else if (trnmtTeamBased == false && trnmtParticipants == 16) writeTournamentPlayer16();
    else if (trnmtTeamBased == false && trnmtParticipants == 8) writeTournamentPlayer8();
    else if (trnmtTeamBased == false && trnmtParticipants == 4) writeTournamentPlayer4();
}


/**
 * @brief Skriver ut turneringsformat for spiller
 */
void Tournament::writeTournamentPlayer16() {
    cout << endl;
    if (tournamentRoundMatch[0]->player1 != nullptr) {
        tournamentRoundMatch[0]->player1->writeGamerTag();
    } else cout << "'Ikke Definert'" << endl;

    if (tournamentQfMatch[0]->player1 != nullptr) {cout << "\t\t\t\t\t"; tournamentQfMatch[0]->player1->writeGamerTag();}
    else cout << "\t\t\t\t\t" << "Vinner av R1" << endl;
    
    if (tournamentRoundMatch[0]->player2 != nullptr) {
        tournamentRoundMatch[0]->player2->writeGamerTag();
    } else cout << "'Ikke Definert'" << endl;
    
    if (tournamentSfMatch[0]->player1 != nullptr) {cout << "\t\t\t\t\t\t\t\t\t\t"; tournamentSfMatch[0]->player1->writeGamerTag();}
    else cout << "\t\t\t\t\t\t\t\t\t\t" << "Vinner av QF1" << endl;
    
    if (tournamentRoundMatch[1]->player1 != nullptr) {
        tournamentRoundMatch[1]->player1->writeGamerTag();
    } else cout << "'Ikke Definert'" << endl;
    
    if (tournamentQfMatch[0]->player2 != nullptr) {cout << "\t\t\t\t\t"; tournamentQfMatch[0]->player2->writeGamerTag();}
    else cout << "\t\t\t\t\t" << "Vinner av R2" << endl;
    
    if (tournamentRoundMatch[1]->player2 != nullptr) {
        tournamentRoundMatch[1]->player2->writeGamerTag();
    } else cout << "'Ikke Definert'" << endl;
    
    if (tournamentGfMatch[0]->player1 != nullptr) {cout << "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t"; tournamentGfMatch[0]->player1->writeGamerTag();}
    else cout << "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t" << "Vinner av SF1" << endl;
    
    if (tournamentRoundMatch[2]->player1 != nullptr) {
        tournamentRoundMatch[2]->player1->writeGamerTag();
    } else cout << "'Ikke Definert'" << endl;
    
    if (tournamentQfMatch[1]->player1 != nullptr) {cout << "\t\t\t\t\t"; tournamentQfMatch[1]->player1->writeGamerTag();}
    else cout << "\t\t\t\t\t" << "Vinner av R3" << endl;
    
    if (tournamentRoundMatch[2]->player2 != nullptr) {
        tournamentRoundMatch[2]->player2->writeGamerTag();
    } else cout << "'Ikke Definert'" << endl;
    
    if (tournamentSfMatch[0]->player2 != nullptr) {cout << "\t\t\t\t\t\t\t\t\t\t"; tournamentSfMatch[0]->player2->writeGamerTag();}
    else cout << "\t\t\t\t\t\t\t\t\t\t" << "Vinner av QF2" << endl;
    
    if (tournamentRoundMatch[3]->player1 != nullptr) {
        tournamentRoundMatch[3]->player1->writeGamerTag();
    } else cout << "'Ikke Definert'" << endl;
    
    if (tournamentQfMatch[1]->player2 != nullptr) {cout << "\t\t\t\t\t"; tournamentQfMatch[1]->player2->writeGamerTag();}
    else cout << "\t\t\t\t\t" << "Vinner av R4" << endl;
    
    if (tournamentRoundMatch[3]->player2 != nullptr) {
        tournamentRoundMatch[3]->player2->writeGamerTag();
    } else cout << "'Ikke Definert'" << endl;
    
    cout << endl;
    
    if (tournamentRoundMatch[4]->player1 != nullptr) {
        tournamentRoundMatch[4]->player1->writeGamerTag();
    } else cout << "'Ikke Definert'" << endl;
    
    if (tournamentQfMatch[2]->player1 != nullptr) {cout << "\t\t\t\t\t"; tournamentQfMatch[2]->player1->writeGamerTag();}
    else cout << "\t\t\t\t\t" << "Vinner av R5" << endl;
    
    if (tournamentRoundMatch[4]->player2 != nullptr) {
        tournamentRoundMatch[4]->player2->writeGamerTag();
    } else cout << "'Ikke Definert'" << endl;
    
    if (tournamentSfMatch[1]->player1 != nullptr) {cout << "\t\t\t\t\t\t\t\t\t\t"; tournamentSfMatch[1]->player1->writeGamerTag();}
    else cout << "\t\t\t\t\t\t\t\t\t\t" << "Vinner av QF3" << endl;
    
    if (tournamentRoundMatch[5]->player1 != nullptr) {
        tournamentRoundMatch[5]->player1->writeGamerTag();
    } else cout << "'Ikke Definert'" << endl;
    
    if (tournamentQfMatch[2]->player2 != nullptr) {cout << "\t\t\t\t\t"; tournamentQfMatch[2]->player2->writeGamerTag();}
    else cout << "\t\t\t\t\t" << "Vinner av R6" << endl;
    
    if (tournamentRoundMatch[5]->player2 != nullptr) {
        tournamentRoundMatch[5]->player2->writeGamerTag();
    } else cout << "'Ikke Definert'" << endl;
    
    if (tournamentGfMatch[0]->player2 != nullptr) {cout << "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t"; tournamentGfMatch[0]->player2->writeGamerTag();}
    else cout << "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t" << "Vinner av SF2" << endl;
    
    if (tournamentRoundMatch[6]->player1 != nullptr) {
        tournamentRoundMatch[6]->player1->writeGamerTag();
    } else cout << "'Ikke Definert'" << endl;
    
    if (tournamentQfMatch[3]->player1 != nullptr) {cout << "\t\t\t\t\t"; tournamentQfMatch[3]->player1->writeGamerTag();}
    else cout << "\t\t\t\t\t" << "Vinner av R7" << endl;
    
    if (tournamentRoundMatch[6]->player2 != nullptr) {
        tournamentRoundMatch[6]->player2->writeGamerTag();
    } else cout << "'Ikke Definert'" << endl;
    
    if (tournamentSfMatch[1]->player2 != nullptr) {cout << "\t\t\t\t\t\t\t\t\t\t"; tournamentSfMatch[1]->player2->writeGamerTag();}
    else cout << "\t\t\t\t\t\t\t\t\t\t" << "Vinner av QF4" << endl;
    
    if (tournamentRoundMatch[7]->player1 != nullptr) {
        tournamentRoundMatch[7]->player1->writeGamerTag();
    } else cout << "'Ikke Definert'" << endl;
    
    if (tournamentQfMatch[3]->player2 != nullptr) {cout << "\t\t\t\t\t"; tournamentQfMatch[3]->player2->writeGamerTag();}
    else cout << "\t\t\t\t\t" << "Vinner av R8" << endl;
    
    if (tournamentRoundMatch[7]->player2 != nullptr) {
        tournamentRoundMatch[7]->player2->writeGamerTag();
    } else cout << "'Ikke Definert'" << endl;
}


/**
 * @brief skriver ut turneringsformat for 8 spillere
 */
void Tournament::writeTournamentPlayer8() {
    cout << endl;
    if (tournamentQfMatch[0]->player1 != nullptr) { tournamentQfMatch[0]->player1->writeGamerTag();}
    else cout << "'Ikke Definert'" << endl;
    
    if (tournamentSfMatch[0]->player1 != nullptr) {cout << "\t\t\t\t\t"; tournamentSfMatch[0]->player1->writeGamerTag();}
    else cout << "\t\t\t\t\t" << "Vinner av QF1" << endl;
    
    if (tournamentQfMatch[0]->player2 != nullptr) { tournamentQfMatch[0]->player2->writeGamerTag();}
    else cout << "'Ikke Definert'" << endl;
    
    if (tournamentGfMatch[0]->player1 != nullptr) {cout << "\t\t\t\t\t\t\t\t\t\t"; tournamentGfMatch[0]->player1->writeGamerTag();}
    else cout << "\t\t\t\t\t\t\t\t\t\t" << "Vinner av SF1" << endl;
    
    if (tournamentQfMatch[1]->player1 != nullptr) { tournamentQfMatch[1]->player1->writeGamerTag();}
    else cout << "'Ikke Definert'" << endl;
    
    if (tournamentSfMatch[0]->player2 != nullptr) {cout << "\t\t\t\t\t"; tournamentSfMatch[0]->player2->writeGamerTag();}
    else cout << "\t\t\t\t\t" << "Vinner av QF2" << endl;
    
    if (tournamentQfMatch[1]->player2 != nullptr) { tournamentQfMatch[1]->player2->writeGamerTag();}
    else cout << "'Ikke Definert'" << endl;
    
    cout << endl;
    
    if (tournamentQfMatch[2]->player1 != nullptr) { tournamentQfMatch[2]->player1->writeGamerTag();}
    else cout << "'Ikke Definert'" << endl;
    
    if (tournamentSfMatch[1]->player1 != nullptr) {cout << "\t\t\t\t\t"; tournamentSfMatch[1]->player1->writeGamerTag();}
    else cout << "\t\t\t\t\t" << "Vinner av QF3" << endl;
    
    if (tournamentQfMatch[2]->player2 != nullptr) { tournamentQfMatch[2]->player2->writeGamerTag();}
    else cout << "'Ikke Definert'" << endl;
    
    if (tournamentGfMatch[0]->player2 != nullptr) {cout << "\t\t\t\t\t\t\t\t\t\t"; tournamentGfMatch[0]->player2->writeGamerTag();}
    else cout << "\t\t\t\t\t\t\t\t\t\t" << "Vinner av SF2" << endl;
    
    if (tournamentQfMatch[3]->player1 != nullptr) { tournamentQfMatch[3]->player1->writeGamerTag();}
    else cout << "'Ikke Definert'" << endl;
    
    if (tournamentSfMatch[1]->player2 != nullptr) {cout << "\t\t\t\t\t"; tournamentSfMatch[1]->player2->writeGamerTag();}
    else cout << "\t\t\t\t\t" << "Vinner av QF4" << endl;
    
    if (tournamentQfMatch[3]->player2 != nullptr) { tournamentQfMatch[3]->player2->writeGamerTag();}
    else cout << "'Ikke Definert'" << endl;

}


/**
 * @brief skriver ut turneringsformat for 4 spillere
 */
void Tournament::writeTournamentPlayer4() {
    cout << endl;
    
    if (tournamentSfMatch[0]->player1 != nullptr) { tournamentSfMatch[0]->player1->writeGamerTag();}
    else cout << "'Ikke Definert'" << endl;
    
    
    if (tournamentGfMatch[0]->player1 != nullptr) {cout << "\t\t\t\t\t"; tournamentGfMatch[0]->player1->writeGamerTag();}
    else cout << "\t\t\t\t\t" << "Vinner av SF1" << endl;
    
    
    if (tournamentSfMatch[0]->player2 != nullptr) { tournamentSfMatch[0]->player2->writeGamerTag();}
    else cout << "'Ikke Definert'" << endl;
    
    cout << endl;
    
    if (tournamentSfMatch[1]->player1 != nullptr) { tournamentSfMatch[1]->player1->writeGamerTag();}
    else cout << "'Ikke Definert'" << endl;
    
    if (tournamentGfMatch[0]->player2 != nullptr) {cout << "\t\t\t\t\t"; tournamentGfMatch[0]->player2->writeGamerTag();}
    else cout << "\t\t\t\t\t" << "Vinner av SF2" << endl;
    
    if (tournamentSfMatch[1]->player2 != nullptr) { tournamentSfMatch[1]->player2->writeGamerTag();}
    else cout << "'Ikke Definert'" << endl;
    
}



/**
 * @brief Skriver ut turneringsformat for 16 lag
 */
void Tournament::writeTournamentTeam16() {
    cout << endl;
    if (tournamentRoundMatch[0]->team1 != nullptr) {
        tournamentRoundMatch[0]->team1->writeName();
    } else cout << "'Ikke Definert'" << endl;

    if (tournamentQfMatch[0]->team1 != nullptr) {cout << "\t\t\t\t\t"; tournamentQfMatch[0]->team1->writeName();}
    else cout << "\t\t\t\t\t" << "Vinner av R1" << endl;
    
    if (tournamentRoundMatch[0]->team2 != nullptr) {
        tournamentRoundMatch[0]->team2->writeName();
    } else cout << "'Ikke Definert'" << endl;
    
    if (tournamentSfMatch[0]->team1 != nullptr) {cout << "\t\t\t\t\t\t\t\t\t\t"; tournamentSfMatch[0]->team1->writeName();}
    else cout << "\t\t\t\t\t\t\t\t\t\t" << "Vinner av QF1" << endl;
    
    if (tournamentRoundMatch[1]->team1 != nullptr) {
        tournamentRoundMatch[1]->team1->writeName();
    } else cout << "'Ikke Definert'" << endl;
    
    if (tournamentQfMatch[0]->team2 != nullptr) {cout << "\t\t\t\t\t"; tournamentQfMatch[0]->team2->writeName();}
    else cout << "\t\t\t\t\t" << "Vinner av R2" << endl;
    
    if (tournamentRoundMatch[1]->team2 != nullptr) {
        tournamentRoundMatch[1]->team2->writeName();
    } else cout << "'Ikke Definert'" << endl;
    
    if (tournamentGfMatch[0]->team1 != nullptr) {cout << "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t"; tournamentGfMatch[0]->team1->writeName();}
    else cout << "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t" << "Vinner av SF1" << endl;
    
    if (tournamentRoundMatch[2]->team1 != nullptr) {
        tournamentRoundMatch[2]->team1->writeName();
    } else cout << "'Ikke Definert'" << endl;
    
    if (tournamentQfMatch[1]->team1 != nullptr) {cout << "\t\t\t\t\t"; tournamentQfMatch[1]->team1->writeName();}
    else cout << "\t\t\t\t\t" << "Vinner av R3" << endl;
    
    if (tournamentRoundMatch[2]->team2 != nullptr) {
        tournamentRoundMatch[2]->team2->writeName();
    } else cout << "'Ikke Definert'" << endl;
    
    if (tournamentSfMatch[0]->team2 != nullptr) {cout << "\t\t\t\t\t\t\t\t\t\t"; tournamentSfMatch[0]->team2->writeName();}
    else cout << "\t\t\t\t\t\t\t\t\t\t" << "Vinner av QF2" << endl;
    
    if (tournamentRoundMatch[3]->team1 != nullptr) {
        tournamentRoundMatch[3]->team1->writeName();
    } else cout << "'Ikke Definert'" << endl;
    
    if (tournamentQfMatch[1]->team2 != nullptr) {cout << "\t\t\t\t\t"; tournamentQfMatch[1]->team2->writeName();}
    else cout << "\t\t\t\t\t" << "Vinner av R4" << endl;
    
    if (tournamentRoundMatch[3]->team2 != nullptr) {
        tournamentRoundMatch[3]->team2->writeName();
    } else cout << "'Ikke Definert'" << endl;
    
    cout << endl;
    
    if (tournamentRoundMatch[4]->team1 != nullptr) {
        tournamentRoundMatch[4]->team1->writeName();
    } else cout << "'Ikke Definert'" << endl;
    
    if (tournamentQfMatch[2]->team1 != nullptr) {cout << "\t\t\t\t\t"; tournamentQfMatch[2]->team1->writeName();}
    else cout << "\t\t\t\t\t" << "Vinner av R5" << endl;
    
    if (tournamentRoundMatch[4]->team2 != nullptr) {
        tournamentRoundMatch[4]->team2->writeName();
    } else cout << "'Ikke Definert'" << endl;
    
    if (tournamentSfMatch[1]->team1 != nullptr) {cout << "\t\t\t\t\t\t\t\t\t\t"; tournamentSfMatch[1]->team1->writeName();}
    else cout << "\t\t\t\t\t\t\t\t\t\t" << "Vinner av QF3" << endl;
    
    if (tournamentRoundMatch[5]->team1 != nullptr) {
        tournamentRoundMatch[5]->team1->writeName();
    } else cout << "'Ikke Definert'" << endl;
    
    if (tournamentQfMatch[2]->team2 != nullptr) {cout << "\t\t\t\t\t"; tournamentQfMatch[2]->team2->writeName();}
    else cout << "\t\t\t\t\t" << "Vinner av R6" << endl;
    
    if (tournamentRoundMatch[5]->team2 != nullptr) {
        tournamentRoundMatch[5]->team2->writeName();
    } else cout << "'Ikke Definert'" << endl;
    
    if (tournamentGfMatch[0]->team2 != nullptr) {cout << "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t"; tournamentGfMatch[0]->team2->writeName();}
    else cout << "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t" << "Vinner av SF2" << endl;
    
    if (tournamentRoundMatch[6]->team1 != nullptr) {
        tournamentRoundMatch[6]->team1->writeName();
    } else cout << "'Ikke Definert'" << endl;
    
    if (tournamentQfMatch[3]->team1 != nullptr) {cout << "\t\t\t\t\t"; tournamentQfMatch[3]->team1->writeName();}
    else cout << "\t\t\t\t\t" << "Vinner av R7" << endl;
    
    if (tournamentRoundMatch[6]->team2 != nullptr) {
        tournamentRoundMatch[6]->team2->writeName();
    } else cout << "'Ikke Definert'" << endl;
    
    if (tournamentSfMatch[1]->team2 != nullptr) {cout << "\t\t\t\t\t\t\t\t\t\t"; tournamentSfMatch[1]->team2->writeName();}
    else cout << "\t\t\t\t\t\t\t\t\t\t" << "Vinner av QF4" << endl;
    
    if (tournamentRoundMatch[7]->team1 != nullptr) {
        tournamentRoundMatch[7]->team1->writeName();
    } else cout << "'Ikke Definert'" << endl;
    
    if (tournamentQfMatch[3]->team2 != nullptr) {cout << "\t\t\t\t\t"; tournamentQfMatch[3]->team2->writeName();}
    else cout << "\t\t\t\t\t" << "Vinner av R8" << endl;
    
    if (tournamentRoundMatch[7]->team2 != nullptr) {
        tournamentRoundMatch[7]->team2->writeName();
    } else cout << "'Ikke Definert'" << endl;
}


/**
 * @brief skriver ut turneringsformat for 8 lag
 */
void Tournament::writeTournamentTeam8() {
    cout << endl;
    if (tournamentQfMatch[0]->team1 != nullptr) { tournamentQfMatch[0]->team1->writeName();}
    else cout << "'Ikke Definert'" << endl;
    
    if (tournamentSfMatch[0]->team1 != nullptr) {cout << "\t\t\t\t\t"; tournamentSfMatch[0]->team1->writeName();}
    else cout << "\t\t\t\t\t" << "Vinner av QF1" << endl;
    
    if (tournamentQfMatch[0]->team2 != nullptr) { tournamentQfMatch[0]->team2->writeName();}
    else cout << "'Ikke Definert'" << endl;
    
    if (tournamentGfMatch[0]->team1 != nullptr) {cout << "\t\t\t\t\t\t\t\t\t\t"; tournamentGfMatch[0]->team1->writeName();}
    else cout << "\t\t\t\t\t\t\t\t\t\t" << "Vinner av SF1" << endl;
    
    if (tournamentQfMatch[1]->team1 != nullptr) { tournamentQfMatch[1]->team1->writeName();}
    else cout << "'Ikke Definert'" << endl;
    
    if (tournamentSfMatch[0]->team2 != nullptr) {cout << "\t\t\t\t\t"; tournamentSfMatch[0]->team2->writeName();}
    else cout << "\t\t\t\t\t" << "Vinner av QF2" << endl;
    
    if (tournamentQfMatch[1]->team2 != nullptr) { tournamentQfMatch[1]->team2->writeName();}
    else cout << "'Ikke Definert'" << endl;
    
    cout << endl;
    
    if (tournamentQfMatch[2]->team1 != nullptr) { tournamentQfMatch[2]->team1->writeName();}
    else cout << "'Ikke Definert'" << endl;
    
    if (tournamentSfMatch[1]->team1 != nullptr) {cout << "\t\t\t\t\t"; tournamentSfMatch[1]->team1->writeName();}
    else cout << "\t\t\t\t\t" << "Vinner av QF3" << endl;
    
    if (tournamentQfMatch[2]->team2 != nullptr) { tournamentQfMatch[2]->team2->writeName();}
    else cout << "'Ikke Definert'" << endl;
    
    if (tournamentGfMatch[0]->team2 != nullptr) {cout << "\t\t\t\t\t\t\t\t\t\t"; tournamentGfMatch[0]->team2->writeName();}
    else cout << "\t\t\t\t\t\t\t\t\t\t" << "Vinner av SF2" << endl;
    
    if (tournamentQfMatch[3]->team1 != nullptr) { tournamentQfMatch[3]->team1->writeName();}
    else cout << "'Ikke Definert'" << endl;
    
    if (tournamentSfMatch[1]->team2 != nullptr) {cout << "\t\t\t\t\t"; tournamentSfMatch[1]->team2->writeName();}
    else cout << "\t\t\t\t\t" << "Vinner av QF4" << endl;
    
    if (tournamentQfMatch[3]->team2 != nullptr) { tournamentQfMatch[3]->team2->writeName();}
    else cout << "'Ikke Definert'" << endl;
}


/**
 * @brief skriver ut turneringsformat for 4 lag
 */
void Tournament::writeTournamentTeam4() {
    cout << endl;
    
    if (tournamentSfMatch[0]->team1 != nullptr) { tournamentSfMatch[0]->team1->writeName();}
    else cout << "'Ikke Definert'" << endl;
    
    
    if (tournamentGfMatch[0]->team1 != nullptr) {cout << "\t\t\t\t\t"; tournamentGfMatch[0]->team1->writeName();}
    else cout << "\t\t\t\t\t" << "Vinner av SF1" << endl;
    
    
    if (tournamentSfMatch[0]->team2 != nullptr) { tournamentSfMatch[0]->team2->writeName();}
    else cout << "'Ikke Definert'" << endl;
    
    cout << endl;
    
    if (tournamentSfMatch[1]->team1 != nullptr) { tournamentSfMatch[1]->team1->writeName();}
    else cout << "'Ikke Definert'" << endl;
    
    if (tournamentGfMatch[0]->team2 != nullptr) {cout << "\t\t\t\t\t"; tournamentGfMatch[0]->team2->writeName();}
    else cout << "\t\t\t\t\t" << "Vinner av SF2" << endl;
    
    if (tournamentSfMatch[1]->team2 != nullptr) { tournamentSfMatch[1]->team2->writeName();}
    else cout << "'Ikke Definert'" << endl;
}


/**
 * @brief hjelpefunksjon til addParticipant. Sender med vektor basert på turneringsstørrelsen
 */
void Tournament::addParticipantHelper() {
    if (trnmtParticipants == 16) addParticipant(tournamentRoundMatch);
    else if (trnmtParticipants == 8) addParticipant(tournamentQfMatch);
    else if (trnmtParticipants == 4) addParticipant(tournamentSfMatch);
    else cout << endl << "Deltakermengde ikke definert" << endl;
    
}

/**
 * @brief Legger til spillere i turneringens første runde
 */
void Tournament::addParticipant(vector <TournamentMatch*> bracketMatch) {
    if (trnmtTeamBased == false) {
        string addPlayer;
        Player* vAddPlayer;
        listAllPlayers();
        for (int i = 0; i<bracketMatch.size()*2; i++) {
            if (bracketMatch[i/2]->player1 == nullptr ) {
                addPlayer = lesString("\nSkriv inn navn til spiller som skal legges til, 'Q' for å avslutte");
                vAddPlayer = findPlayer(addPlayer);
                
                if (vAddPlayer != nullptr) {
                    bracketMatch[i/2]->player1 = vAddPlayer; }
                
                else if (addPlayer == "Q") {
                    cout << endl << "Returnerer..." << endl;
                    break; }
                
                else {
                    cout << endl << "Gamertag ikke funnet, legger ikke til" << endl;
                    i--; }
                
            } else if (bracketMatch[i/2]->player2 == nullptr) {
                addPlayer = lesString("\nSkriv inn navn til laget som skal legges til, 'Q' for å avslutte");
                vAddPlayer = findPlayer(addPlayer);
                
                if (vAddPlayer != nullptr) {
                    bracketMatch[i/2]->player2 = vAddPlayer; }
                
                else if (addPlayer == "Q") {
                    cout << endl << "Returnerer..." << endl;
                    break; }
                
                else {
                    cout << endl << "Gamertag ikke funnet, legger ikke til" << endl;
                    i--; }
            }
        }
    } else if (trnmtTeamBased == true) {
        string addTeam;
        Team* vAddTeam;
        listAllTeams();
        for (int i = 0; i<bracketMatch.size()*2; i++) {
            if (bracketMatch[i/2]->team1 == nullptr ) {
                addTeam = lesString("\nSkriv inn navn til laget som skal legges til, 'Q' for å avslutte");
                vAddTeam = findTeam(addTeam);
                
                if (vAddTeam != nullptr) {
                    bracketMatch[i/2]->team1 = vAddTeam; }
                
                else if (addTeam == "Q") {
                    cout << endl << "Returnerer..." << endl;
                    break; }
                
                else {
                    cout << endl << "Gamertag ikke funnet, legger ikke til" << endl;
                    i--; }
                
            } else if (bracketMatch[i/2]->team2 == nullptr) {
                addTeam = lesString("\nSkriv inn navn til laget som skal legges til, 'Q' for å avslutte");
                vAddTeam = findTeam(addTeam);
                
                if (vAddTeam != nullptr) {
                    bracketMatch[i/2]->team2 = vAddTeam; }
                
                else if (addTeam == "Q") {
                    cout << endl << "Returnerer..." << endl;
                    break; }
                
                else {
                    cout << endl << "Gamertag ikke funnet, legger ikke til" << endl;
                    i--; }
            }
        } cout << endl << "Turneringen er full, returnerer..." << endl;
    }
}


/**
 * @brief Hjelpefunksjon til removeParticipant
 */
void Tournament::removeParticipantInput() {
    listAllParticipants();
    string participantToDelete;
    Player* playerToDelete;
    Team* teamToDelete;
    
    do {
        participantToDelete = lesString("\nDeltaker som skal fjernes, 'Q' for å avslutte");
        if (trnmtTeamBased == false) {
            playerToDelete = findPlayer(participantToDelete);
            if (playerToDelete != nullptr) {
                removeParticipantHelper(participantToDelete);
                
            } else if (participantToDelete == "Q") cout << endl << "Returnerer..." << endl;
            else cout << endl << "Deltaker eksisterer ikke..." << endl;
            
        } else {
            teamToDelete = findTeam(participantToDelete);
            if (teamToDelete != nullptr) {
                removeParticipantHelper(participantToDelete);
                
            } else if (participantToDelete == "Q") cout << endl << "Returnerer..." << endl;
            else cout << endl << "Deltaker eksisterer ikke..." << endl;
        }
    } while (participantToDelete != "Q");
    
}


/**
 * @brief sender videre deltakeren med turneringens runde vectorer
 * @param participantToDelete deltakeren som skal slettes.
 */
void Tournament::removeParticipantHelper(string participantToDelete) {
    
    if (trnmtParticipants == 16) { removeParticipant(tournamentRoundMatch, participantToDelete);}
    if (trnmtParticipants >= 8) {
        removeParticipant(tournamentQfMatch, participantToDelete);}
    if (trnmtParticipants >= 4) {
        removeParticipant(tournamentSfMatch, participantToDelete);}
    removeParticipant(tournamentGfMatch, participantToDelete);
}



/**
 * @brief Fjerner spiller fra turneringern
 * @param bracketMatch vectoren som skal sjekkes for inneholdende deltaker.
 * @param participant deltakeren
 */
void Tournament::removeParticipant(vector <TournamentMatch*> bracketMatch, string participant) {
    if (trnmtTeamBased == false) {
        for (int i = 0; i<bracketMatch.size(); i++) {
            if (bracketMatch[i]->player1 == findPlayer(participant)) {
                bracketMatch[i]->player1 = nullptr;
            } else if (bracketMatch[i]->player2 == findPlayer(participant)) {
                bracketMatch[i]->player2 = nullptr;
            }
        }
    } else {
        for (int i = 0; i<bracketMatch.size(); i++) {
            if (bracketMatch[i]->team1 == findTeam(participant)) {
                bracketMatch[i]->team1 = nullptr;
            } else if (bracketMatch[i]->team2 == findTeam(participant)) {
                bracketMatch[i]->team2 = nullptr;
            }
        }
    }
}


/**
 * @brief lister alle turneringens deltakere
 */
void Tournament::listAllParticipants() {
    if (trnmtTeamBased == false) {
        cout << "\nSkriver ut alle turneringens deltakende spillere:" << endl;
        if (trnmtParticipants == 16) {
            for (int i = 0; i<tournamentRoundMatch.size(); i++) {
                if (tournamentRoundMatch[i]->player1 != nullptr) {
                    cout <<"\t"<<tournamentRoundMatch[i]->player1->returnGamerTag() << endl;
                } if (tournamentRoundMatch[i]->player2 != nullptr) {
                    cout <<"\t"<<tournamentRoundMatch[i]->player2->returnGamerTag() << endl;
                }
            }
            
        } else if (trnmtParticipants == 8) {
            for (int i = 0; i<tournamentQfMatch.size(); i++) {
                if (tournamentQfMatch[i]->player1 != nullptr) {
                    cout <<"\t"<<tournamentQfMatch[i]->player1->returnGamerTag() << endl;
                } if (tournamentQfMatch[i]->player2 != nullptr) {
                    cout <<"\t"<<tournamentQfMatch[i]->player2->returnGamerTag() << endl;
                }
            }
            
        } else if (trnmtParticipants == 4) {
            for (int i = 0; i<tournamentSfMatch.size(); i++) {
                if (tournamentSfMatch[i]->player1 != nullptr) {
                    cout <<"\t"<<tournamentSfMatch[i]->player1->returnGamerTag() << endl;
                } if (tournamentSfMatch[i]->player2 != nullptr) {
                    cout <<"\t"<<tournamentSfMatch[i]->player2->returnGamerTag() << endl;
                }
            }
        }
        
    } else {
        cout << "\nSkriver ut alle turneringens deltakende lag:" << endl;
        if (trnmtParticipants == 16) {
            for (int i = 0; i<tournamentRoundMatch.size(); i++) {
                if (tournamentRoundMatch[i]->team1 != nullptr) {
                    cout <<"\t"<<tournamentRoundMatch[i]->team1->returnName() << endl;
                } if (tournamentRoundMatch[i]->team2 != nullptr) {
                    cout <<"\t"<<tournamentRoundMatch[i]->team2->returnName() << endl;
                }
            }
            
        } else if (trnmtParticipants == 8) {
            for (int i = 0; i<tournamentQfMatch.size(); i++) {
                if (tournamentQfMatch[i]->team1 != nullptr) {
                    cout <<"\t"<<tournamentQfMatch[i]->team1->returnName() << endl;
                } if (tournamentQfMatch[i]->team2 != nullptr) {
                    cout <<"\t"<<tournamentQfMatch[i]->team2->returnName() << endl;
                }
            }
        } else if (trnmtParticipants == 4) {
            for (int i = 0; i<tournamentSfMatch.size(); i++) {
                if (tournamentSfMatch[i]->team1 != nullptr) {
                    cout <<"\t"<<tournamentSfMatch[i]->team1->returnName() << endl;
                } if (tournamentSfMatch[i]->team2 != nullptr) {
                    cout <<"\t"<<tournamentSfMatch[i]->team2->returnName() << endl;
                }
            }
        }
    }
}


/**
 * @brief sjekker om turneringen inneholder gitt lag eller spiller
 */
bool Tournament::hasPlayerOrTeam(string participant) {
    if (trnmtTeamBased == false) {
        if (trnmtParticipants == 16) {
            for (int i = 0; i<tournamentRoundMatch.size(); i++) {
                
                if (tournamentRoundMatch[i]->player1 == findPlayer(participant)) {
                    return true;
                } else if (tournamentRoundMatch[i]->player2 == findPlayer(participant)) {
                    return true;
                }
            }
            
        } else if (trnmtParticipants == 8) {
            for (int i = 0; i<tournamentQfMatch.size(); i++) {
                if (tournamentQfMatch[i]->player1 == findPlayer(participant)) {
                    return true;
                } else if (tournamentQfMatch[i]->player2 == findPlayer(participant)) {
                    return true;
                }
            }
            
        } else if (trnmtParticipants == 4) {
            for (int i = 0; i<tournamentSfMatch.size(); i++) {
                if (tournamentSfMatch[i]->player1 == findPlayer(participant)) {
                    return true;
                } if (tournamentSfMatch[i]->player2 == findPlayer(participant)) {
                    return true;
                }
            }
        }
        
    } else {
        if (trnmtParticipants == 16) {
            for (int i = 0; i<tournamentRoundMatch.size(); i++) {
                if (tournamentRoundMatch[i]->team1 == findTeam(participant)) {
                    return true;
                } if (tournamentRoundMatch[i]->team2 == findTeam(participant)) {
                    return true;
                }
            }
            
        } else if (trnmtParticipants == 8) {
            for (int i = 0; i<tournamentQfMatch.size(); i++) {
                if (tournamentQfMatch[i]->team1 == findTeam(participant)) {
                    return true;
                } if (tournamentQfMatch[i]->team2 == findTeam(participant)) {
                    return true;
                }
            }
        } else if (trnmtParticipants == 4) {
            for (int i = 0; i<tournamentSfMatch.size(); i++) {
                if (tournamentSfMatch[i]->team1 == findTeam(participant)) {
                    return true;
                } if (tournamentSfMatch[i]->team2 == findTeam(participant)) {
                    return true;
                }
            }
        }
    }
    return false;
}


/**
 * @brief Hjelpefunksjon til findTournament.
 * @param findTournament Sjekker om turnering har samme gamertag som param.
 */
bool Tournament::hasName (const string findTournament) const {
    if (trnmtName == findTournament) return true;
    else return false;
}


/**
 * @brief Skriver ut turneringens navn
 */
void Tournament::writeName() {
    cout << trnmtName << endl;
}


/**
 * @brief returnerer turneringsnavnet
 */
string Tournament::returnName() {
    return trnmtName;
}


/**
 * @brief returnerer om laget er lagbasert eller ei
 */
bool Tournament::returnTeamBased() {
    return trnmtTeamBased;
}


/**
 * @brief returnerer mengden lag/spillere i turneringen (turneringsstørrelse)
 */
int Tournament::returnTrnmtParticipants() {
    return trnmtParticipants;
}


/**
 * @brief Writes tournamentdata to file
 * @param outputFile file to be written to
 */
void Tournament::writeTournamentToFile(ofstream &outputFile) {
    outputFile << trnmtName << ';' << trnmtTeamBased << ';' << trnmtParticipants << ';' << endl;
    outputFile << roundMatchesPlayed << ';' << qfMatchesPlayed << ';' << sfMatchesPlayed << ';' << gfMatchesPlayed << ';' << endl;
    
    writeTournamentToFileHelper(outputFile, tournamentRoundMatch);
    writeTournamentToFileHelper(outputFile, tournamentQfMatch);
    writeTournamentToFileHelper(outputFile, tournamentSfMatch);
    writeTournamentToFileHelper(outputFile, tournamentGfMatch);
    
    cout << "\t" << trnmtName << " skrevet til fil." << endl;
}


/**
 * @brief hjelpefunksjon til Tournament::writeTournamentToFile
 * @param outputFile filen det skrives til
 * @param bracketMatch turneringsrunden som skal skrives
 */
void Tournament::writeTournamentToFileHelper(ofstream &outputFile, vector <TournamentMatch*> bracketMatch) {
    for (int i = 0; i<bracketMatch.size(); i++) {
        if ( trnmtTeamBased == false) {
                
            if (bracketMatch[i]->player1 != nullptr) {
                outputFile << bracketMatch[i]->player1->returnGamerTag() << ';';
            } else {outputFile << "IkkeDefinert" << ';'; }
            
            if (bracketMatch[i]->player2 != nullptr) {
                outputFile << bracketMatch[i]->player2->returnGamerTag() << ';';
            } else outputFile << "IkkeDefinert" << ';';
                
            if (bracketMatch[i]->player1 != nullptr) {
                outputFile << bracketMatch[i]->matchPlayed << ';';
            } else outputFile << '0' << ';';
                
            if ((i+1)%2 == 0) outputFile << endl;
            else if (i == bracketMatch.size()-1 ) outputFile << endl;
        }
            
        else if (trnmtTeamBased == true) {
            
            if (bracketMatch[i]->team1 != nullptr) {
            outputFile << bracketMatch[i]->team1->returnName() << ';';
            } else outputFile << "IkkeDefinert" << ';';
                
            if (bracketMatch[i]->team2 != nullptr) {
            outputFile << bracketMatch[i]->team2->returnName() << ';';
            } else outputFile << "IkkeDefinert" << ';';
                
            if (bracketMatch[i]->team1 != nullptr) {
                outputFile << bracketMatch[i]->matchPlayed << ';';
            } else outputFile << '0' << ';';
            
            if ((i+1)%2 == 0) outputFile << endl;
            else if (i == bracketMatch.size()-1 ) outputFile << endl;
        }
    }
}


/**
 * @brief hjelpefunksjon for constructor som leser fra fil.
 */
void Tournament::constructorReadFromFile(ifstream &inputFile, vector <TournamentMatch*> bracketMatch) {
    string tempGamerTag;
    bool tempMatchPlayed;
    
    for (int i = 0; i<bracketMatch.size(); i++) {
        
        getline(inputFile, tempGamerTag, ';');
        if (tempGamerTag != "IkkeDefinert") {
            if (trnmtTeamBased == false) {
                bracketMatch[i]->player1 = findPlayer(tempGamerTag); }
            else {
                bracketMatch[i]->team1 = findTeam(tempGamerTag); }
        } else {
            bracketMatch[i]->player1 = nullptr;
            bracketMatch[i]->team1 = nullptr;
        }
        
        getline(inputFile, tempGamerTag, ';');
        if (tempGamerTag != "IkkeDefinert") {
            if (trnmtTeamBased == false) {
                bracketMatch[i]->player2 = findPlayer(tempGamerTag); }
            else {
                bracketMatch[i]->team2 = findTeam(tempGamerTag); }
        } else {
            bracketMatch[i]->player2 = nullptr;
            bracketMatch[i]->team2 = nullptr;
        }
        
        if (tempGamerTag != "IkkeDefinert") {
            inputFile >> bracketMatch[i]->matchPlayed;
        } else inputFile >> tempMatchPlayed; inputFile.ignore(1,';');
        
        if ((i+1)%2 == 0) inputFile.ignore(2,'\n');
        if (bracketMatch == tournamentGfMatch) inputFile.ignore(2,'\n');
    }
}

//                          PLAY TOURNAMENT                       //
//****************************************************************//


/**
 * @brief skriver meny for spilling av turnering
 */
void Tournament::writePlayTournamentMenu() {
    cout << "\nHva vil du gjøre?" << endl;
    cout << "\tS: Spill Neste match" << endl;
    cout << "\tL: Liste opp gjenværende matcher for runde" << endl;
    cout << "\tT: Se turneringsbracket" << endl;
    cout << "\tQ: Returner" << endl;
}

/**
 * @brief meny for spilling av turnering.
 */
void Tournament::playTournamentMenu() {
    writePlayTournamentMenu();
    char playValg = lesChar("Kommando: ");
    
    while (playValg != 'Q') {
        switch (playValg) {
            case 'S': playNextTournamentMatch();       break;
            case 'L': listRemainingMatches();          break;
            case 'T': writeTournament();               break;
            case 'Q': cout << "\nReturnerer...";       break;
            default: writePlayTournamentMenu();        break;
        }
        writePlayTournamentMenu();
        playValg = lesChar("Kommando: ");
    }
}


/**
 * @brief Lister opp gjenværende kamper i angitt matchtier (Runde, QF, SF eller GF)
 */
void Tournament::listRemainingMatches() {
    if (roundMatchesPlayed != roundMatchesTotal) {
        for (int i = 0; i<tournamentRoundMatch.size(); i++) {
            if (tournamentRoundMatch[i]->matchPlayed == false) {
                cout << endl << "Rundematch " << i+1 << ":";
                if (tournamentRoundMatch[i]->player1 != nullptr) {
                    cout << " " << tournamentRoundMatch[i]->player1->returnGamerTag() << " VS";
                } else if (tournamentRoundMatch[i]->team1 != nullptr) {
                    cout << " " << tournamentRoundMatch[i]->team1->returnName() << " VS";
                } else cout << "'Ikke Definert' VS";
                
                if (tournamentRoundMatch[i]->player2 != nullptr) {
                    cout << " " << tournamentRoundMatch[i]->player2->returnGamerTag() << endl << endl;
                } else if (tournamentRoundMatch[i]->team2 != nullptr) {
                    cout << " " << tournamentRoundMatch[i]->team2->returnName() << endl << endl;
                } else cout << " 'Ikke Definert'" << endl << endl;
            }
        }
    }
    else if (qfMatchesPlayed != qfMatchesTotal) {
        for (int i = 0; i<tournamentQfMatch.size(); i++) {
            if (tournamentQfMatch[i]->matchPlayed == false) {
                cout << endl << "Kvartfinale match " << i+1 << ":";
                if (tournamentQfMatch[i]->player1 != nullptr) {
                    cout << " " << tournamentQfMatch[i]->player1->returnGamerTag() << " VS";
                } else if (tournamentQfMatch[i]->team1 != nullptr) {
                    cout << " " << tournamentQfMatch[i]->team1->returnName() << " VS";
                } else cout << "'Ikke Definert' VS";
                
                if (tournamentQfMatch[i]->player2 != nullptr) {
                    cout << " " << tournamentQfMatch[i]->player2->returnGamerTag() << endl << endl;
                } else if (tournamentQfMatch[i]->team2 != nullptr) {
                    cout << " " << tournamentQfMatch[i]->team2->returnName() << endl << endl;
                } else cout << " 'Ikke Definert'" << endl << endl;
            }
        }
    }
    else if (sfMatchesPlayed != sfMatchesTotal) {
        for (int i = 0; i<tournamentSfMatch.size(); i++) {
            if (tournamentSfMatch[i]->matchPlayed == false) {
                cout << endl << "Semifinale match " << i+1 << ":";
                if (tournamentSfMatch[i]->player1 != nullptr) {
                    cout << " " << tournamentSfMatch[i]->player1->returnGamerTag() << " VS";
                } else if (tournamentSfMatch[i]->team1 != nullptr) {
                    cout << " " << tournamentSfMatch[i]->team1->returnName() << " VS";
                } else cout << "'Ikke Definert' VS";
                
                if (tournamentSfMatch[i]->player2 != nullptr) {
                    cout << " " << tournamentSfMatch[i]->player2->returnGamerTag() << endl << endl;
                } else if (tournamentSfMatch[i]->team2 != nullptr) {
                    cout << " " << tournamentSfMatch[i]->team2->returnName() << endl << endl;
                } else cout << " 'Ikke Definert'" << endl << endl;
            }
        }
    }
    else if (gfMatchesPlayed != gfMatchesTotal) {
        cout << endl << "Grand Finals: " << endl;
        if (tournamentGfMatch[0]->player1 != nullptr) {
            cout << " " << tournamentGfMatch[0]->player1->returnGamerTag() << " VS";
        } else if (tournamentGfMatch[0]->team1 != nullptr) {
            cout << " " << tournamentGfMatch[0]->team1->returnName() << " VS";
        } else cout << "'Ikke Definert' VS";
        
        if (tournamentGfMatch[0]->player2 != nullptr) {
            cout << " " << tournamentGfMatch[0]->player2->returnGamerTag() << endl << endl;
        } else if (tournamentGfMatch[0]->team2 != nullptr) {
            cout << " " << tournamentGfMatch[0]->team2->returnName() << endl << endl;
        } else cout << " 'Ikke Definert'" << endl << endl;
        
    } else cout << endl << "Turneringen er fullført! returnerer..." << endl;
}


/**
 * @brief Spiller neste match i turneringsformatet
 */
void Tournament::playNextTournamentMatch() {
if (playNextTournamentBracketMatch(tournamentRoundMatch, tournamentQfMatch, roundMatchesPlayed, roundMatchesTotal) == true){
        if (playNextTournamentBracketMatch(tournamentQfMatch, tournamentSfMatch, qfMatchesPlayed, qfMatchesTotal) == true){
            if (playNextTournamentBracketMatch(tournamentSfMatch, tournamentGfMatch, sfMatchesPlayed, sfMatchesTotal) == true){
                if (playNextTournamentGfMatch() == true){
                    cout << endl << "Turneringen er fullført!" << endl;}
            } else {}
                    
        } else {}
    }
}

/**
 * @brief Spiller neste kamp i turneringens runde 1
 * @return returnerer true hvis det skal fortsettes til turneringens andre runde (QF), false hvis alle kamper ikke er spilt eller det ikke skal fortsettes.
 */
bool Tournament::playNextTournamentBracketMatch(vector <TournamentMatch*> currentBracketMatch, vector <TournamentMatch*> nextBracketMatch, int& bracketMatchesPlayed, const int bracketMatchesTotal) {
    char playValg;
    
    if (roundMatchesPlayed == roundMatchesTotal && currentBracketMatch == tournamentRoundMatch) return true;
    if (qfMatchesPlayed == qfMatchesTotal && currentBracketMatch == tournamentQfMatch) return true;
    if (sfMatchesPlayed == sfMatchesTotal && currentBracketMatch == tournamentSfMatch) return true;
    
    
    if (tournamentMatchHasPlayerNullPtr(currentBracketMatch) == false) {
        // Går gjennom denne hvis ikke alle turneringens første matcher er spilt
        if (bracketMatchesPlayed != bracketMatchesTotal) {
            for (int i = 0; i<currentBracketMatch.size(); i++) {
                if (currentBracketMatch[i]->matchPlayed != true) {
        
                    cout << endl << "Spiller rundematch " << i+1 << " ";
                    cout << "Spiller 1: " << currentBracketMatch[i]->player1->returnGamerTag();
                    cout << " VS Spiller 2: " << currentBracketMatch[i]->player2->returnGamerTag();
                    int vinner = lesInt("\nSkriv inn spiller som vant, 1 eller 2:", 1, 2);
            
                    // Flytter valgt (Spiller 1) opp til neste tier bracket
                    if (vinner == 1) {
                        if (bracketMatchesPlayed%2 == 0) {
                            nextBracketMatch[i/2]->player1 = currentBracketMatch[i]->player1;
                            currentBracketMatch[i]->player1->plusMatchWins(false);
                            currentBracketMatch[i]->player2->plusMatchesPlayed(true);
                            currentBracketMatch[i]->matchPlayed = true;
                            bracketMatchesPlayed++;
                        
                            //Gir bruker mulighet til å avslutte
                            if (bracketMatchesPlayed == bracketMatchesTotal) {
                                playValg = lesChar("\nSkriv ett vilkårlig tegn for å fortsette til neste turneringsrunde, eller 'Q' for å avslutte");
                                if (playValg == 'Q') return false;
                                else return true; }
                        
                            else {
                                playValg = lesChar("\nSkriv ett vilkårlig tegn for å fortsette til neste kamp, eller 'Q' for å avslutte");
                                if (playValg == 'Q') return false; }}
                    
                        else {
                            nextBracketMatch[i/2]->player2 = currentBracketMatch[i]->player1;
                            currentBracketMatch[i]->player1->plusMatchWins(false);
                            currentBracketMatch[i]->player2->plusMatchesPlayed(true);
                            currentBracketMatch[i]->matchPlayed = true;
                            bracketMatchesPlayed++;
                        
                            //Gir bruker mulighet til å avslutte
                            if (bracketMatchesPlayed == bracketMatchesTotal) {
                                playValg = lesChar("\nSkriv ett vilkårlig tegn for å fortsette til neste turneringsrunde, eller 'Q' for å avslutte");
                                if (playValg == 'Q') return false;
                                else return true; }
                            else {
                                playValg = lesChar("\nSkriv ett vilkårlig tegn for å fortsette til neste kamp, eller 'Q' for å avslutte");
                                if (playValg == 'Q') return false; }}
                    }
            
                    // Flytter valgt (Spiller 2) opp til neste tier bracket
                    else {
                        if (bracketMatchesPlayed%2 == 0) {
                            nextBracketMatch[i/2]->player1 = currentBracketMatch[i]->player2;
                            currentBracketMatch[i]->player2->plusMatchWins(false);
                            currentBracketMatch[i]->player1->plusMatchesPlayed(true);
                            currentBracketMatch[i]->matchPlayed = true;
                            bracketMatchesPlayed++;
                        
                            //Gir bruker mulighet til å avslutte
                            if (bracketMatchesPlayed == bracketMatchesTotal) {
                                playValg = lesChar("\nSkriv ett vilkårlig tegn for å fortsette til neste turneringsrunde, eller 'Q' for å avslutte");
                                if (playValg == 'Q') return false;
                                else return true; }
                            else {
                                playValg = lesChar("\nSkriv ett vilkårlig tegn for å fortsette til neste kamp, eller 'Q' for å avslutte");
                                if (playValg == 'Q') return false; }}
                    
                        else {
                            nextBracketMatch[i/2]->player2 = currentBracketMatch[i]->player2;
                            currentBracketMatch[i]->player2->plusMatchWins(false);
                            currentBracketMatch[i]->player1->plusMatchesPlayed(true);
                            currentBracketMatch[i]->matchPlayed = true;
                            bracketMatchesPlayed++;
                        
                            //Gir bruker mulighet til å avslutte
                            if (bracketMatchesPlayed == bracketMatchesTotal) {
                                playValg = lesChar("\nSkriv ett vilkårlig tegn for å fortsette til neste turneringsrunde, eller 'Q' for å avslutte");
                                if (playValg == 'Q') return false;
                                else return true; }
                            else {
                                playValg = lesChar("\nSkriv ett vilkårlig tegn for å fortsette til neste kamp, eller 'Q' for å avslutte");
                                if (playValg == 'Q') return false; }
                        }
                    }
                } // IF MatchPlayed brace
            } // For loop brace
        } return true;
    } // Første IF - Finnes det spillere i structen?
    
    // Hvis noen av spillerne var nullptr sjekkes team for nullptr
    else if (tournamentMatchHasTeamNullPtr(currentBracketMatch) == false) {
        // Går gjennom denne hvis ikke alle turneringens første matcher er spilt
        if (bracketMatchesPlayed != bracketMatchesTotal) {
            for (int i = 0; i<currentBracketMatch.size(); i++) {
                if (currentBracketMatch[i]->matchPlayed != true) {
        
                    cout << endl << "Spiller rundematch " << i+1 << " ";
                    cout << "Spiller 1: " << currentBracketMatch[i]->team1->returnName();
                    cout << " VS Spiller 2: " << currentBracketMatch[i]->team2->returnName();
                    int vinner = lesInt("\nSkriv inn spiller som vant, 1 eller 2:", 1, 2);
            
                    // Flytter valgt (Spiller 1) opp til neste tier bracket
                    if (vinner == 1) {
                        if (bracketMatchesPlayed%2 == 0) {
                            nextBracketMatch[i/2]->team1 = currentBracketMatch[i]->team1;
                            currentBracketMatch[i]->team1->plusMatchWins(false);
                            currentBracketMatch[i]->team2->plusMatchesPlayed(true);
                            currentBracketMatch[i]->matchPlayed = true;
                            bracketMatchesPlayed++;
                        
                            //Gir bruker mulighet til å avslutte
                            if (bracketMatchesPlayed == bracketMatchesTotal) {
                                playValg = lesChar("\nSkriv ett vilkårlig tegn for å fortsette til neste turneringsrunde, eller 'Q' for å avslutte");
                                if (playValg == 'Q') return false;
                                else return true; }
                        
                            else {
                                playValg = lesChar("\nSkriv ett vilkårlig tegn for å fortsette til neste kamp, eller 'Q' for å avslutte");
                                if (playValg == 'Q') return false; }}
                    
                        else {
                            nextBracketMatch[i/2]->team2 = currentBracketMatch[i]->team1;
                            currentBracketMatch[i]->team1->plusMatchWins(false);
                            currentBracketMatch[i]->team2->plusMatchesPlayed(true);
                            currentBracketMatch[i]->matchPlayed = true;
                            bracketMatchesPlayed++;
                        
                            //Gir bruker mulighet til å avslutte
                            if (bracketMatchesPlayed == bracketMatchesTotal) {
                                playValg = lesChar("\nSkriv ett vilkårlig tegn for å fortsette til neste turneringsrunde, eller 'Q' for å avslutte");
                                if (playValg == 'Q') return false;
                                else return true; }
                            else {
                                playValg = lesChar("\nSkriv ett vilkårlig tegn for å fortsette til neste kamp, eller 'Q' for å avslutte");
                                if (playValg == 'Q') return false; }}
                    }
            
                    // Flytter valgt (Spiller 2) opp til neste tier bracket
                    else {
                        if (bracketMatchesPlayed%2 == 0) {
                            nextBracketMatch[i/2]->team1 = currentBracketMatch[i]->team2;
                            currentBracketMatch[i]->team2->plusMatchWins(false);
                            currentBracketMatch[i]->team1->plusMatchesPlayed(true);
                            currentBracketMatch[i]->matchPlayed = true;
                            bracketMatchesPlayed++;
                        
                            //Gir bruker mulighet til å avslutte
                            if (bracketMatchesPlayed == bracketMatchesTotal) {
                                playValg = lesChar("\nSkriv ett vilkårlig tegn for å fortsette til neste turneringsrunde, eller 'Q' for å avslutte");
                                if (playValg == 'Q') return false;
                                else return true; }
                            else {
                                playValg = lesChar("\nSkriv ett vilkårlig tegn for å fortsette til neste kamp, eller 'Q' for å avslutte");
                                if (playValg == 'Q') return false; }}
                    
                        else {
                            nextBracketMatch[i/2]->team2 = currentBracketMatch[i]->team2;
                            currentBracketMatch[i]->team2->plusMatchWins(false);
                            currentBracketMatch[i]->team1->plusMatchesPlayed(true);
                            currentBracketMatch[i]->matchPlayed = true;
                            bracketMatchesPlayed++;
                        
                            //Gir bruker mulighet til å avslutte
                            if (bracketMatchesPlayed == bracketMatchesTotal) {
                                playValg = lesChar("\nSkriv ett vilkårlig tegn for å fortsette til neste turneringsrunde, eller 'Q' for å avslutte");
                                if (playValg == 'Q') return false;
                                else return true; }
                            else {
                                playValg = lesChar("\nSkriv ett vilkårlig tegn for å fortsette til neste kamp, eller 'Q' for å avslutte");
                                if (playValg == 'Q') return false; }
                        }
                    }
                } // IF MatchPlayed brace
            } // For loop brace
        } return true;
        
    }
    else return false;
}

/**
 * @brief sjekker om noen av spiller vectorene er nullpointere, skriver feilmelding om nullptr er funnet
 * @param bracketMatchToCheck vectoren som skal sjekkes for nullptr
 */
bool Tournament::tournamentMatchHasPlayerNullPtr(vector <TournamentMatch*> bracketMatchToCheck) {
    for (int i = 0; i<bracketMatchToCheck.size(); i++) {
        if (bracketMatchToCheck[i] != nullptr) {
            if (bracketMatchToCheck[i]->player1 == nullptr) {
                return true;
            }
            else if (bracketMatchToCheck[i]->player2 == nullptr) {
                return true;
            }
        } else {
            cout << endl << "bracketMatchToCheck[" << i << "]returnerer 'nullptr'"<< endl;
            return true;
        }
    }
    return false;
}


/**
 * @brief sjekker om noen av spiller vectorene er nullpointere, skriver feilmelding om nullptr er funnet
 * @param bracketMatchToCheck vectoren som skal sjekkes for nullptr
 */
bool Tournament::tournamentMatchHasTeamNullPtr(vector <TournamentMatch*> bracketMatchToCheck) {
    
    for (int i = 0; i<bracketMatchToCheck.size(); i++) {
        if (bracketMatchToCheck[i] != nullptr) {
            if (bracketMatchToCheck[i]->team1 == nullptr) {
                return true;
            }
            else if (bracketMatchToCheck[i]->team2 == nullptr) {
                return true;
            }
        } else {
            cout << endl << "bracketMatchToCheck[" << i << "]returnerer 'nullptr'"<< endl;
            return true;
        }
    }
    return false;
}

/**
 * @brief Spiller turneringens finale
 */
bool Tournament::playNextTournamentGfMatch() {
    if (tournamentGfMatch[0]->matchPlayed != true) {

        if (tournamentMatchHasPlayerNullPtr(tournamentGfMatch) == false) {
            cout << endl << "Spiller finale";
            cout << "Spiller 1: " << tournamentGfMatch[0]->player1->returnName();
            cout << " VS Spiller 2: " << tournamentGfMatch[0]->player2->returnName();
            int vinner = lesInt("\nSkriv inn spiller som vant, 1 eller 2:", 1, 2);
            if (vinner == 1) {
                tournamentGfMatch[0]->matchPlayed = true;
                tournamentGfMatch[0]->player1->plusMatchWins(true);
                tournamentGfMatch[0]->player2->plusMatchesPlayed(true);
                gfMatchesPlayed++;
                cout << endl << "Gratulerer, " << tournamentGfMatch[0]->player1->returnName();
                cout << " vant turneringens finale!" << endl;
                return true;
            } else {
                tournamentGfMatch[0]->matchPlayed = true;
                tournamentGfMatch[0]->player2->plusMatchWins(true);
                tournamentGfMatch[0]->player1->plusMatchesPlayed(true);
                gfMatchesPlayed++;
                cout << endl << "Gratulerer, " << tournamentGfMatch[0]->player2->returnName();
                cout << " vant turneringens finale!" << endl;
                return true;
            }
        } else if (tournamentMatchHasTeamNullPtr(tournamentGfMatch) == false) {
            cout << endl << "Spiller finale";
            cout << "Lag 1: " << tournamentGfMatch[0]->team1->returnName();
            cout << " VS Lag 2: " << tournamentGfMatch[0]->team2->returnName();
            int vinner = lesInt("\nSkriv inn spiller som vant, 1 eller 2:", 1, 2);
            if (vinner == 1) {
                tournamentGfMatch[0]->matchPlayed = true;
                tournamentGfMatch[0]->team1->plusMatchWins(true);
                tournamentGfMatch[0]->team2->plusMatchesPlayed(true);
                gfMatchesPlayed++;
                cout << endl << "Gratulerer, " << tournamentGfMatch[0]->team1->returnName();
                cout << " vant turneringens finale!" << endl;
                return true;
            } else {
                tournamentGfMatch[0]->matchPlayed = true;
                tournamentGfMatch[0]->team2->plusMatchWins(true);
                tournamentGfMatch[0]->team1->plusMatchesPlayed(true);
                gfMatchesPlayed++;
                cout << endl << "Gratulerer, " << tournamentGfMatch[0]->team2->returnName();
                cout << " vant turneringens finale!" << endl;
                return true;
            }
        } return false;
    }
    cout << endl << "Turneringen er fullført! (Mangler lagring av vinner hehe..." << endl;
    return true;
}
