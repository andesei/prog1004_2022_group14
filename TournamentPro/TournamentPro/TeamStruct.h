//
//  TeamStruct.h
//  TournamentPro
//
//  Created by Karl-Henrik Horve on 20/04/2022.
//

#ifndef TeamStruct_h
#define TeamStruct_h

#include <vector>
#include "Player.h"

struct TeamMembers {
    int memberAmount;
    std::vector <Player*> teamMember;                       //Liste med peker til lagmedlemmer
};


#endif /* TeamStruct_h */
