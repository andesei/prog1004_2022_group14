//
//  TournamentStruct.h
//  TournamentPro
//
//  Created by Karl-Henrik Horve on 20/04/2022.
//

#ifndef TournamentStruct_h
#define TournamentStruct_h

#include "Player.h"
#include "Team.h"

struct TournamentMatch {
    Player* player1;
    Player* player2;
    Team* team1;
    Team* team2;
    bool matchPlayed;
};
#endif /* TournamentStruct_h */
