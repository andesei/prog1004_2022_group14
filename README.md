# README.md

The program can be launched through the TournamentPro.Exe file on a Windows based computer.


[See this link for the product manual](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Product%20Manual/productManual.md)


The program requires the following files placed within the same folder as the executable:
'PLAYERDATA.DTA'
'TEAMDATA.DTA'
'TOURNAMENTDATA.DTA'


The files either need to be empty, IE containing only '0' on the first line, or they need to have properly formatted data according to [Persistence](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/blob/main/Dokumentasjon/Persistence/Persistence.md) guidelines. Any data input inside the program is automatically stored in the correct format.


For any other information [see this link](https://gitlab.stud.idi.ntnu.no/andesei/prog1004_2022_group14/-/wikis/_sidebar)

Warning: As the program was written in an OS X environment, some hiccups are currently expected on a windows machine.
